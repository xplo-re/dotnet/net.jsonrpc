﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Runtime.InteropServices;
using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc />
    /// <summary>
    ///     Base exception class for JSON-RPC exceptions.
    /// </summary>
    [ComVisible(true)]
    public class JsonRpcException : Exception
    {

        private const string DefaultErrorMessage = "Error while handling a JSON-RPC message.";

        // Use explicit private member to avoid virtual property setter calls in constructors.
        private readonly JsonRpcId _messageId;

        // ReSharper disable once ConvertToAutoProperty
        /// <summary>
        ///     The JSON-RPC ID of the message related to the exception.
        /// </summary>
        [CanBeNull]
        public virtual JsonRpcId MessageId => _messageId;

        /// <inheritdoc />
        /// <summary>
        ///     Gets the error message including the JSON-RPC ID, if known.
        /// </summary>
        [CanBeNull]
        public override string Message
        {
            get {
                var message = base.Message;

                if (_messageId == null ||
                    _messageId.Type == JsonRpcIdType.None) {
                    return message;
                }

                return message + Environment.NewLine + $"The ID of the related JSON-RPC message is '{_messageId}'.";
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcException" /> instance with the default error message.
        /// </summary>
        public JsonRpcException()
            : base(DefaultErrorMessage)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcException" /> instance with the default error message and a 
        ///     reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcException([CanBeNull] Exception innerException)
            : base(DefaultErrorMessage, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcException" /> instance with a specified error message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public JsonRpcException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcException" /> instance with the default error message and the
        ///     ID of the related JSON-RPC message.
        /// </summary>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        public JsonRpcException([CanBeNull] JsonRpcId id)
            : this(DefaultErrorMessage, id)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcException" /> instance with a specified error message and a 
        ///     reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcException(string message, [CanBeNull] Exception innerException)
            : base(message, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcException" /> instance with the default error message, the ID of the
        ///     related JSON-RPC message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcException([CanBeNull] JsonRpcId id, [CanBeNull] Exception innerException)
            : this(DefaultErrorMessage, id, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcException" /> instance with a specified error message and the ID 
        ///     of the related JSON-RPC message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        public JsonRpcException(string message, [CanBeNull] JsonRpcId id)
            : base(message)
        {
            _messageId = id;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcException" /> instance with a specified error message, the ID of the
        ///     related JSON-RPC message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcException(string message, [CanBeNull] JsonRpcId id, [CanBeNull] Exception innerException)
            : base(message, innerException)
        {
            _messageId = id;
        }

    }

}
