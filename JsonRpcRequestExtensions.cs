﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc
{

    /// <summary>
    ///     Provides status methods and response factories for <see cref="JsonRpcRequest" /> instances.
    /// </summary>
    public static class JsonRpcRequestExtensions
    {

        /// <summary>
        ///     Determines whether the invoked method is a RPC-internal or extension method.
        /// </summary>
        /// <param name="request">This <see cref="IJsonRpcRequest" /> instance.</param>
        /// <returns>
        ///     <c>true</c>, if the method of the request is a RPC-internal or extension method, otherwise <c>false</c>.
        /// </returns>
        [Pure]
        public static bool IsInternal([NotNull] this IJsonRpcRequest request)
        {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            return request.Method.StartsWith(JsonRpcRequest.InternalMethodPrefix);
        }

        /// <summary>
        ///     Determines whether the request is a notification, i.e. doesn't contain a JSON-RPC message ID and will
        ///     generate no response from the server.
        /// </summary>
        /// <param name="request">This <see cref="IJsonRpcRequest" /> instance.</param>
        /// <returns><c>true</c>, if the the request is a notification, otherwise <c>false</c>.</returns>
        [Pure]
        public static bool IsNotification([NotNull] this IJsonRpcRequest request)
        {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            return request.Id.Type == JsonRpcIdType.None;
        }


        #region Response Factories

        /// <summary>
        ///     Creates a new <see cref="JsonRpcResponse{TResult}" /> instance with the same JSON-RPC ID as this
        ///     <see cref="JsonRpcRequest" /> and the provided result object.
        /// </summary>
        /// <param name="request">This <see cref="IJsonRpcRequest" /> instance.</param>
        /// <param name="result">The result object of the response.</param>
        /// <typeparam name="T">The type of the <paramref name="result" /> parameter.</typeparam>
        /// <returns>A JSON-RPC response with a matching ID that contains the provided result object.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> CreateResponse<T>([NotNull] this IJsonRpcRequest request, T result)
        {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            return JsonRpcResponse.WithResult(result, request.Id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcResponse" /> instance with the same JSON-RPC ID as this
        ///     <see cref="JsonRpcRequest" /> and the provided error details.
        /// </summary>
        /// <param name="request">This <see cref="IJsonRpcRequest" /> instance.</param>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <returns>A JSON-RPC response with a matching ID that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse CreateResponseWithError(
            [NotNull] this IJsonRpcRequest request,
            int code, [NotNull] string message)
        {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            return JsonRpcResponse.WithError(code, message, request.Id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcResponse" /> instance with the same JSON-RPC ID as this
        ///     <see cref="JsonRpcRequest" /> and the provided error details.
        /// </summary>
        /// <param name="request">This <see cref="IJsonRpcRequest" /> instance.</param>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <param name="data">
        ///     An optional primitive or structured value with additional information about the error.
        /// </param>
        /// <returns>A JSON-RPC response with a matching ID that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse CreateResponseWithError(
            [NotNull] this IJsonRpcRequest request,
            int code, [NotNull] string message, [CanBeNull] object data)
        {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            return JsonRpcResponse.WithError(code, message, data, request.Id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcResponse" /> instance with the same JSON-RPC ID as this
        ///     <see cref="JsonRpcRequest" /> and the provided error details.
        /// </summary>
        /// <param name="request">This <see cref="IJsonRpcRequest" /> instance.</param>
        /// <param name="jsonRpcError">The <see cref="JsonRpcError" /> instance that describes the error.</param>
        /// <returns>A JSON-RPC response with a matching ID that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse CreateResponseWithError(
            [NotNull] this IJsonRpcRequest request,
            [NotNull] JsonRpcError jsonRpcError)
        {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            return JsonRpcResponse.WithError(jsonRpcError, request.Id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcResponse{TResult}" /> instance with the same JSON-RPC ID as this
        ///     <see cref="JsonRpcRequest" /> and the provided error details.
        /// </summary>
        /// <param name="request">This <see cref="IJsonRpcRequest" /> instance.</param>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <typeparam name="T">The desired type of the reponse result.</typeparam>
        /// <returns>A JSON-RPC response with a matching ID that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> CreateResponseWithError<T>(
            [NotNull] this IJsonRpcRequest request,
            int code, [NotNull] string message)
        {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            return JsonRpcResponse.WithError<T>(code, message, request.Id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcResponse{TResult}" /> instance with the same JSON-RPC ID as this
        ///     <see cref="JsonRpcRequest" /> and the provided error details.
        /// </summary>
        /// <param name="request">This <see cref="IJsonRpcRequest" /> instance.</param>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <param name="data">
        ///     An optional primitive or structured value with additional information about the error.
        /// </param>
        /// <typeparam name="T">The desired type of the reponse result.</typeparam>
        /// <returns>A JSON-RPC response with a matching ID that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> CreateResponseWithError<T>(
            [NotNull] this IJsonRpcRequest request,
            int code, [NotNull] string message, [CanBeNull] object data)
        {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            return JsonRpcResponse.WithError<T>(code, message, data, request.Id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcResponse{TResult}" /> instance with the same JSON-RPC ID as this
        ///     <see cref="JsonRpcRequest" /> and the provided error details.
        /// </summary>
        /// <param name="request">This <see cref="IJsonRpcRequest" /> instance.</param>
        /// <param name="jsonRpcError">The <see cref="JsonRpcError" /> instance that describes the error.</param>
        /// <typeparam name="T">The desired type of the reponse result.</typeparam>
        /// <returns>A JSON-RPC response with a matching ID that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> CreateResponseWithError<T>(
            [NotNull] this IJsonRpcRequest request,
            [NotNull] JsonRpcError jsonRpcError)
        {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            return JsonRpcResponse.WithError<T>(jsonRpcError, request.Id);
        }

        #endregion

    }

}
