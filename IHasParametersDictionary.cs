﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections.Generic;
using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc
{

    /// <summary>
    ///     Provides a read-only dictionary of parameter names and their corresponding parameter values. 
    /// </summary>
    public interface IHasParametersDictionary
    {

        /// <summary>
        ///     Immutable key-value dictionary of parameters.
        /// </summary>
        [NotNull]
        IReadOnlyDictionary<string, object> Parameters { get; }

    }

}
