﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using Newtonsoft.Json;
using XploRe.Net.JsonRpc.Internal;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc />
    /// <summary>
    ///     Provides additional JSON-RPC specific serialisation settings.
    /// </summary>
    public class JsonRpcSerializerSettings : JsonSerializerSettings
    {

        /// <summary>
        ///     Expected JSON-RPC protocol version of messages.
        /// </summary>
        public string ProtocolVersion { get; set; } = JsonRpcInternals.ProtocolVersion;

        /// <summary>
        ///     Configures the treatment of messages with a JSON-RPC protocol version that differs from the configured
        ///     JSON-RPC protocol version.
        /// </summary>
        public JsonRpcProtocolCompatiblityMode ProtocolCompatiblityMode { get; set; } =
            JsonRpcProtocolCompatiblityMode.Relaxed;

    }

}
