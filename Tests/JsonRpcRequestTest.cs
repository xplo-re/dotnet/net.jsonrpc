﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Text;
using FluentAssertions;
using FluentAssertions.Json;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using XploRe.Runtime;
using Xunit;


// ReSharper disable AssignNullToNotNullAttribute
// ReSharper disable PossibleNullReferenceException

namespace XploRe.Net.JsonRpc.Tests
{

    public class JsonRpcRequestTest
    {

        internal class Generator
        {

            /// <summary>
            ///     Yields JSON-RPC request objects with their expected JSON payload.
            /// </summary>
            public static IEnumerable<object[]> JsonRepresentations
            {
                [UsedImplicitly]
                get {
                    yield return new object[] {
                        JsonRpcRequest.WithMethod("test.omitParams", new object[] { }, JsonRpcId.FromNumber(1)),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""method"": ""test.omitParams"",
                            ""id"": 1
                        }"
                    };

                    yield return new object[] {
                        JsonRpcRequest.WithMethod("test.omitParams.nullID", new object[] { }, JsonRpcId.Null),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""method"": ""test.omitParams.nullID"",
                            ""id"": null
                        }"
                    };

                    yield return new object[] {
                        JsonRpcRequest.NotificationWithMethod("test.omitParams.notification"),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""method"": ""test.omitParams.notification""
                        }"
                    };

                    yield return new object[] {
                        JsonRpcRequest.WithMethod(
                            "test.ParamsList.Simple",
                            new object[] { 42, 24 },
                            JsonRpcId.FromNumber(1)
                        ),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""method"": ""test.ParamsList.Simple"",
                            ""params"": [ 42, 24 ],
                            ""id"": 1
                        }"
                    };

                    yield return new object[] {
                        JsonRpcRequest.WithMethod(
                            "test.ParamsList.Nested",
                            new object[] { 42, new[] { "a", "b" } },
                            JsonRpcId.FromNumber(1)
                        ),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""method"": ""test.ParamsList.Nested"",
                            ""params"": [ 42, [ ""a"", ""b"" ] ],
                            ""id"": 1
                        }"
                    };

                    yield return new object[] {
                        JsonRpcRequest.WithMethod(
                            "test.ParamsDict.Simple",
                            new Dictionary<string, object> { { "p1", 42 }, { "p2", 24 } },
                            JsonRpcId.FromNumber(1)
                        ),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""method"": ""test.ParamsDict.Simple"",
                            ""params"": { ""p1"": 42, ""p2"": 24 },
                            ""id"": 1
                        }"
                    };

                    yield return new object[] {
                        JsonRpcRequest.WithMethod(
                            "test.ParamsDict.Nested",
                            new Dictionary<string, object> { { "p1", 42 }, { "p2", new[] { "a", "b" } } },
                            JsonRpcId.FromNumber(1)
                        ),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""method"": ""test.ParamsDict.Nested"",
                            ""params"": { ""p1"": 42, ""p2"": [ ""a"", ""b"" ] },
                            ""id"": 1
                        }"
                    };
                }
            }

            /// <summary>
            ///     Yields valid parameters for factory methods that are empty.
            /// </summary>
            public static IEnumerable<object[]> EmptyParameterDictionaries
            {
                [UsedImplicitly]
                get {
                    yield return new object[] { "test", null };
                    yield return new object[] { "test", new Dictionary<string, object>(0) };
                }
            }

            /// <summary>
            ///     Yields valid parameters for factory methods that are empty.
            /// </summary>
            public static IEnumerable<object[]> EmptyParameterCollections
            {
                [UsedImplicitly]
                get {
                    yield return new object[] { "test", null };
                    yield return new object[] { "test", new object[] { } };
                    yield return new object[] { "test", new List<object>(0) };
                }
            }

            /// <summary>
            ///     Yields valid parameter dictionaries for factory methods that are not empty.
            /// </summary>
            public static IEnumerable<object[]> NonEmptyParameterDictionaries
            {
                [UsedImplicitly]
                get {
                    var mixed = new object[] { 1, "string", 4.5f, new[] { 42, 24 } };
                    var index = 0;

                    yield return new object[] {
                        "test",
                        mixed.ToDictionary(key => "index" + index++, value => value)
                    };

                    yield return new object[] {
                        "test",
                        mixed.ToImmutableDictionary(key => "index" + index++, value => value)
                    };

                    yield return new object[] {
                        "test",
                        new Dictionary<string, object> {
                            { "methodA", 123 },
                            { "methodB", 5.4f },
                            { "methodC", "parameter message string" },
                            { "methodD", new KeyValuePair<string, string>("a", "b") }
                        }
                    };
                }
            }

            /// <summary>
            ///     Yields valid parameter collections for factory methods that are not empty.
            /// </summary>
            public static IEnumerable<object[]> NonEmptyParameterCollections
            {
                [UsedImplicitly]
                get {
                    var mixed = new object[] { 1, "string", 4.5f, new[] { 42, 24 } };

                    // Explicitly create object arrays as otherwise array cannot be converted to IEnumerable<object>.
                    yield return new object[] { "test", new object[] { 1, 2, 3 } };
                    yield return new object[] { "test", new object[] { 1L, 2L } };
                    yield return new object[] { "test", new object[] { 1.1 } };
                    yield return new object[] { "test", new object[] { "a", "b" } };
                    yield return new object[] { "test", mixed };
                    yield return new object[] { "test", new List<object>(mixed) };
                    yield return new object[] { "test", mixed.ToImmutableList() };
                    yield return new object[] { "test", mixed.ToImmutableHashSet() };
                }
            }

            /// <summary>
            ///     Yields valid method names and whether those are considered internal methods.
            /// </summary>
            public static IEnumerable<object[]> ValidMethodNames
            {
                [UsedImplicitly]
                get {
                    yield return new object[] { "test", false };
                    yield return new object[] { "rpc.text", true };
                    yield return new object[] { "rpc", false };
                    yield return new object[] { "rpc.rpc", true };
                }
            }

        }


        #region Factories

        [Theory]
        [MemberData(nameof(Generator.EmptyParameterCollections), MemberType = typeof(Generator))]
        public void FactoryFromEnumerableWithEmptyParametersTest(
            [NotNull] string name,
            [CanBeNull] IEnumerable<object> parameters)
        {
            var request = JsonRpcRequest.WithMethod(name, parameters);

            request.Should().NotBeNull();
            request.IsNotification().Should().BeFalse();
            request.Method.Should().Be(name);
            // Empty parameters automatically yield the base class, no specialised instance.
            request.Should().BeOfType<JsonRpcRequest>();
        }

        [Theory]
        [MemberData(nameof(Generator.EmptyParameterCollections), MemberType = typeof(Generator))]
        public void FactoryFromCollectionWithEmptyParametersTest(
            [NotNull] string name,
            [CanBeNull] ICollection<object> parameters)
        {
            var request = JsonRpcRequest.WithMethod(name, parameters);

            request.Should().NotBeNull();
            request.IsNotification().Should().BeFalse();
            request.Method.Should().Be(name);
            // Empty parameters automatically yield the base class, no specialised instance.
            request.Should().BeOfType<JsonRpcRequest>();
        }

        [Theory]
        [MemberData(nameof(Generator.EmptyParameterDictionaries), MemberType = typeof(Generator))]
        public void FactoryFromDictionaryWithEmptyParametersTest(
            [NotNull] string name,
            [CanBeNull] IDictionary<string, object> parameters)
        {
            var request = JsonRpcRequest.WithMethod(name, parameters);

            request.Should().NotBeNull();
            request.IsNotification().Should().BeFalse();
            request.Method.Should().Be(name);
            // Empty parameters automatically yield the base class, no specialised instance.
            request.Should().BeOfType<JsonRpcRequest>();
        }

        [Theory]
        [MemberData(nameof(Generator.ValidMethodNames), MemberType = typeof(Generator))]
        public void FactoryInternalMethodTest([NotNull] string name, bool isInternal)
        {
            var request = JsonRpcRequest.WithMethod(name);

            request.Should().NotBeNull();
            request.IsInternal().Should().Be(isInternal);
        }

        [Theory]
        [MemberData(nameof(Generator.EmptyParameterCollections), MemberType = typeof(Generator))]
        [MemberData(nameof(Generator.NonEmptyParameterCollections), MemberType = typeof(Generator))]
        public void FactoryNotificationFromEnumerableTest(
            [NotNull] string name,
            [CanBeNull] IEnumerable<object> parameters)
        {
            var request = JsonRpcRequest.NotificationWithMethod(name, parameters);

            request.Should().NotBeNull();
            request.IsNotification().Should().BeTrue();
            request.Method.Should().Be(name);
        }

        [Theory]
        [MemberData(nameof(Generator.EmptyParameterDictionaries), MemberType = typeof(Generator))]
        [MemberData(nameof(Generator.NonEmptyParameterDictionaries), MemberType = typeof(Generator))]
        public void FactoryNotificationFromDictionaryTest(
            [NotNull] string name,
            [CanBeNull] IDictionary<string, object> parameters)
        {
            var request = JsonRpcRequest.NotificationWithMethod(name, parameters);

            request.Should().NotBeNull();
            request.IsNotification().Should().BeTrue();
            request.Method.Should().Be(name);
        }

        [Fact]
        public void FactoryNullParameterTest()
        {
            var request = JsonRpcRequest.WithMethod("test");

            request.Should().NotBeNull();
            request.IsNotification().Should().BeFalse();
            // Empty parameters automatically yield the base class, no specialised instance.
            request.Should().BeOfType<JsonRpcRequest>();
        }

        #endregion


        #region Serialization

        [Theory]
        [MemberData(nameof(Generator.JsonRepresentations), MemberType = typeof(Generator))]
        public void JsonSerializeTest(JsonRpcRequest rpcRequest, string expectedJson)
        {
            var serializedObjectText = JsonConvert.SerializeObject(rpcRequest);
            serializedObjectText.Should().NotBeNull();

            // Compare generated JSON.
            var expectedToken = JToken.Parse(expectedJson);
            var actualToken = JToken.Parse(serializedObjectText);
            actualToken.Should().BeEquivalentTo(expectedToken);

            // Object deserialised from JSON should be equivalent to original. As a specific converter is used, the
            // returned object should be of the same sub-type as the original request object.
            var deserializedObject = JsonConvert.DeserializeObject<JsonRpcRequest>(serializedObjectText);
            deserializedObject.ShouldBeEquivalentTo(rpcRequest);
        }

        [Theory]
        [MemberData(nameof(Generator.JsonRepresentations), MemberType = typeof(Generator))]
        public void ConvertDeserializeUsingStreamReader(JsonRpcRequest rpcRequest, string expectedJson)
        {
            var reader = new StreamReader(expectedJson.ToMemoryStream(Encoding.UTF8));
            reader.Should().NotBeNull();

            // Object deserialised from JSON should be equivalent to original.
            var deserializedObject = JsonRpcConvert.Deserialize<JsonRpcRequest>(reader);
            deserializedObject.ShouldBeEquivalentTo(rpcRequest);
        }

        #endregion

    }

}
