﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using FluentAssertions;
using JetBrains.Annotations;
using Xunit;


// ReSharper disable PossibleNullReferenceException
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace XploRe.Net.JsonRpc.Tests
{

    public class JsonRpcWebClientTest
    {

        private class TestEntity
        {

            [UsedImplicitly]
            public Guid Guid { get; set; } = Guid.NewGuid();

            public string Name { get; set; }
            public int Age { get; set; } = -1;
            public IList<string> Items { get; set; }

        }

        private class TestRandomOrgUsageEntity
        {

            public string Status { get; set; }
            public string CreationTime { get; set; }
            public ulong BitsLeft { get; set; }
            public ulong RequestsLeft { get; set; }
            public ulong TotalBits { get; set; }
            public ulong TotalRequests { get; set; }

        }

        private static class Generator
        {

            public static IEnumerable<object[]> RaboofRequestsAndResponses
            {
                [UsedImplicitly]
                get {
                    // Echoes the text sent as a positional parameter.
                    yield return new object[] {
                        JsonRpcRequest.WithMethod("echo", new[] { "JSON-RPC rules!" }, StaticNumericId),
                        JsonRpcResponse.WithResult("JSON-RPC rules!", StaticNumericId)
                    };

                    // Echoes the text sent as a named parameter.
                    yield return new object[] {
                        JsonRpcRequest.WithMethod(
                            "echo", new Dictionary<string, object> {
                                ["text"] = "JSON-RPC rules!"
                            }, StaticNumericId),
                        JsonRpcResponse.WithResult("JSON-RPC rules!", StaticNumericId)
                    };

                    // Echoes the text sent as a positional parameter, using a random string ID.
                    yield return new object[] {
                        JsonRpcRequest.WithMethod("echo", new[] { "JSON-RPC rules!" }, StaticRandomId),
                        JsonRpcResponse.WithResult("JSON-RPC rules!", StaticRandomId)
                    };

                    // Returns the arbitrary object sent as a parameter.
                    {
                        var entity = new TestEntity {
                            Name = "Test Entity",
                            Age = 42,
                            Items = new List<string> { "Alpha", "Beta", "Gamma" }
                        };

                        yield return new object[] {
                            JsonRpcRequest.WithMethod("echoObject", new[] { entity }, StaticNumericId),
                            JsonRpcResponse.WithResult(entity, StaticNumericId)
                        };
                    }

                    // Invalid method, expects a corresponding error result.
                    yield return new object[] {
                        JsonRpcRequest.WithMethod("invalid.method", StaticNumericId),
                        JsonRpcResponse.WithError<string>(
                            default(int) /* Does not return an error code. */,
                            "Method not found.",
                            StaticNumericId
                        )
                    };
                }
            }
            
            public static IEnumerable<object[]> RandomOrgRequestsAndResponses
            {
                [UsedImplicitly]
                get {
                    // Default example API key, which exists, and returns a data set.
                    yield return new object[] {
                        JsonRpcRequest.WithMethod(
                            "getUsage",  new Dictionary<string, object> {
                                ["apiKey"] = "00000000-0000-0000-0000-000000000000"
                            }, StaticNumericId),
                        JsonRpcResponse.WithResult(new TestRandomOrgUsageEntity {
                                Status = "running",
                                CreationTime = "2013-02-01 17:53:40Z"
                            }, StaticNumericId),
                        false
                    };
                    
                    // Non-existing API key, yields custom error 400 with null data.
                    yield return new object[] {
                        JsonRpcRequest.WithMethod(
                            "getUsage",  new Dictionary<string, object> {
                                ["apiKey"] = "ffffffff-ffff-ffff-ffff-ffffffffffff"
                            }, StaticNumericId),
                        JsonRpcResponse.WithError<string>(
                            400,
                            "The API key you specified does not exist", 
                            null,
                            StaticNumericId),
                        true
                    };

                    // Invalid method. Yields a standard method-not-found error result.
                    yield return new object[] {
                        JsonRpcRequest.WithMethod("invalid", new[] { "Test" }, StaticNumericId),
                        JsonRpcResponse.WithError<string>(
                            JsonRpcErrorCodes.MethodNotFound,
                            "Method not found",
                            StaticNumericId
                            ),
                        true
                    };
                }
            }

        }

        [Theory]
        [MemberData(nameof(Generator.RaboofRequestsAndResponses), MemberType = typeof(Generator))]
        public async void ExpectedRequestResponseRaboof<T>(
            [NotNull] JsonRpcRequest request,
            JsonRpcResponse<T> expectedResponse)
        {
            var settings = new JsonRpcSerializerSettings {
                // Force use of relaxed compatibility mode as Raboof returns messages that are not RFC 2.0 conform.
                ProtocolCompatiblityMode = JsonRpcProtocolCompatiblityMode.Relaxed
            };
            var response = await RaboofClient.InvokeAsync<T>(request, settings);

            response.Should().NotBeNull();
            // Exclude the JSON-RPC protocol version as Raboof does not embed a protocol version in responses.
            response.ShouldBeEquivalentTo(expectedResponse, options => options.Excluding(msg => msg.ProtocolVersion));
        }

        [Theory]
        [MemberData(nameof(Generator.RandomOrgRequestsAndResponses), MemberType = typeof(Generator))]
        public async void ExpectedRequestResponseRandomOrg<T>(
            [NotNull] JsonRpcRequest request,
            JsonRpcResponse<T> expectedResponse,
            bool isError)
        {
            var settings = new JsonRpcSerializerSettings {
                // Random.org is JSON-RPC 2.0 conform.
                ProtocolCompatiblityMode = JsonRpcProtocolCompatiblityMode.Strict
            };
            var response = await RandomOrgClient.InvokeAsync<T>(request, settings);

            response.Should().NotBeNull();

            if (!isError) {
                if (response.Result is TestRandomOrgUsageEntity entity) {
                    entity.ShouldBeEquivalentTo(
                        expectedResponse.Result, 
                        options => options.Excluding(e=>e.BitsLeft)
                                          .Excluding(e=>e.TotalBits)
                                          .Excluding(e=>e.RequestsLeft)
                                          .Excluding(e=>e.TotalRequests),
                        "response entity should match in status and creation date"
                    );
                }
                else {
                    response.Result.Should().Be(expectedResponse.Result, "response entity should match");
                }
                
                response.Error.Should().Be(expectedResponse.Error, "response error should match");
                response.Id.Should().Be(expectedResponse.Id, "response ID should match");
                response.ProtocolVersion.Should().Be(expectedResponse.ProtocolVersion, "response protocol version should match");
            }
            else {
                response.ShouldBeEquivalentTo(expectedResponse);
            }
        }


        #region Static IDs

        [NotNull]
        private static JsonRpcId StaticNumericId { get; } = JsonRpcId.FromNumber(42);

        [NotNull]
        private static JsonRpcId StaticRandomId { get; } = JsonRpcId.Random();

        #endregion


        #region Web Clients

        /// <summary>
        ///     JSON-RPC demo service for the Jayrock library.
        /// </summary>
        /// <see href="http://www.raboof.com/projects/jayrock/demo.ashx" />
        /// <see href="http://www.raboof.com/projects/jayrock/demo.ashx?test" />
        [NotNull]
        private static JsonRpcWebClient RaboofClient =>
            new JsonRpcWebClient(new Uri("http://www.raboof.com/projects/jayrock/demo.ashx"));

        /// <summary>
        ///     JSON-RPC test service that provides multiple generators (not used here) and requires an API key. Only
        ///     the API key usage method is queried which does not require an API key itself.
        /// </summary>
        /// <see href="https://api.random.org/json-rpc/2" />
        [NotNull]
        private static JsonRpcWebClient RandomOrgClient =>
            new JsonRpcWebClient(new Uri("https://api.random.org/json-rpc/2/invoke"));

        #endregion

    }

}
