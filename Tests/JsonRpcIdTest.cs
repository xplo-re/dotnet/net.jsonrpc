/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using FluentAssertions;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Xunit;


// ReSharper disable PossibleNullReferenceException

namespace XploRe.Net.JsonRpc.Tests
{

    public class JsonRpcIdGenerator
    {

        /// <summary>
        ///     Yields valid string IDs.
        /// </summary>
        public static IEnumerable<object[]> ValidStringIds
        {
            [UsedImplicitly]
            get {
                // Arbitrary text ID.
                yield return new object[] { "test-id" };
                // Integer as a string. Should remain a string.
                yield return new object[] { "123" };
            }
        }

        /// <summary>
        ///     Yields invalid string IDs and their corresponding expected exception type.
        /// </summary>
        public static IEnumerable<object[]> InvalidStringIds
        {
            [UsedImplicitly]
            get { yield return new object[] { null, typeof(ArgumentNullException) }; }
        }

        /// <summary>
        ///    Yields valid integer IDs.
        /// </summary>
        public static IEnumerable<object[]> ValidIntegerIds
        {
            [UsedImplicitly]
            get {
                yield return new object[] { 1 };
                yield return new object[] { 12345 };
                yield return new object[] { 0 };
                yield return new object[] { -12345 };
            }
        }

        /// <summary>
        ///     Yields valid numeric IDs with fractions.
        /// </summary>
        public static IEnumerable<object[]> ValidFractionIds
        {
            [UsedImplicitly]
            get {
                yield return new object[] { 1.0f };
                yield return new object[] { 1.2345d };
                yield return new object[] { 0.0d };
                yield return new object[] { -1.2345d };
            }
        }

        /// <summary>
        ///     Yields JSON fragments with their corresponding JSON-RPC ID equivalents.
        /// </summary>
        public static IEnumerable<object[]> JsonFragments
        {
            [UsedImplicitly]
            get {
                yield return new object[] { "null", JsonRpcId.Null };
                yield return new object[] { "0", JsonRpcId.FromNumber(0) };
                yield return new object[] { "0.0", JsonRpcId.FromNumber(0.0) };
                yield return new object[] { "-42", JsonRpcId.FromNumber(-42) };
                yield return new object[] { "-42.42", JsonRpcId.FromNumber(-42.42) };
                yield return new object[] { "'test'", JsonRpcId.FromString("test") };
                yield return new object[] { "'123'", JsonRpcId.FromString("123") };
            }
        }

    }

    public class JsonRpcIdTest
    {

        #region Factories

        [Theory]
        [MemberData(nameof(JsonRpcIdGenerator.ValidStringIds), MemberType = typeof(JsonRpcIdGenerator))]
        public void ValidStringIdFactoryTest([NotNull] string id)
        {
            var messageId = JsonRpcId.FromString(id);

            messageId.Should().NotBeNull();
            messageId.Type.Should().Be(JsonRpcIdType.String);
            messageId.Value.Should().Be(id);
        }

#pragma warning disable xUnit1026

        [Theory]
        [MemberData(nameof(JsonRpcIdGenerator.InvalidStringIds), MemberType = typeof(JsonRpcIdGenerator))]
        public void InvalidStringIdFactoryTest([CanBeNull] string id, [NotNull] Type exceptionType)
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            Action factorize = () => JsonRpcId.FromString(id);

            // TODO: Needs to be updated to check for exceptionType once FluentAssertions supports generic parameters.
            factorize.ShouldThrowExactly<ArgumentNullException>();
        }

#pragma warning restore xUnit1026

        [Theory]
        [MemberData(nameof(JsonRpcIdGenerator.ValidIntegerIds), MemberType = typeof(JsonRpcIdGenerator))]
        public void ValidIntegerIdFactoryTest(long id)
        {
            var messageId = JsonRpcId.FromNumber(id);

            messageId.Should().NotBeNull();
            messageId.Type.Should().Be(JsonRpcIdType.Number);
            messageId.Value.Should().Be(id);
        }

        [Theory]
        [MemberData(nameof(JsonRpcIdGenerator.ValidFractionIds), MemberType = typeof(JsonRpcIdGenerator))]
        public void ValidFractionIdFactoryTest(double id)
        {
            var messageId = JsonRpcId.FromNumber(id);

            messageId.Should().NotBeNull();
            messageId.Type.Should().Be(JsonRpcIdType.Number);
            messageId.Value.Should().Be(id);
        }

        [Fact]
        public void ValidNullIdTest()
        {
            var messageId = JsonRpcId.Null;

            messageId.Should().NotBeNull();
            messageId.Type.Should().Be(JsonRpcIdType.Null);
            messageId.Value.Should().BeNull();
        }

        [Fact]
        public void ValidNoneIdTest()
        {
            var messageId = JsonRpcId.None;

            messageId.Should().NotBeNull();
            messageId.Type.Should().Be(JsonRpcIdType.None);
            messageId.Value.Should().BeNull();
        }

        #endregion


        #region  Serialization

        [Theory]
        [MemberData(nameof(JsonRpcIdGenerator.ValidStringIds), MemberType = typeof(JsonRpcIdGenerator))]
        public void StringIdSerializationTest([NotNull] string id)
        {
            var messageId = JsonRpcId.FromString(id);
            var output = JsonConvert.SerializeObject(messageId, Formatting.Indented);

            output.Should().Be(JsonConvert.SerializeObject(id, Formatting.Indented));
        }

        [Theory]
        [MemberData(nameof(JsonRpcIdGenerator.ValidIntegerIds), MemberType = typeof(JsonRpcIdGenerator))]
        public void IntegerIdSerializationTest(long id)
        {
            var messageId = JsonRpcId.FromNumber(id);
            var output = JsonConvert.SerializeObject(messageId, Formatting.Indented);

            output.Should().Be(JsonConvert.SerializeObject(id, Formatting.Indented));
        }

        [Theory]
        [MemberData(nameof(JsonRpcIdGenerator.ValidFractionIds), MemberType = typeof(JsonRpcIdGenerator))]
        public void DoubleIdSerializationTest(double id)
        {
            var messageId = JsonRpcId.FromNumber(id);
            var output = JsonConvert.SerializeObject(messageId, Formatting.Indented);

            output.Should().Be(JsonConvert.SerializeObject(id, Formatting.Indented));
        }

        [Fact]
        public void NullIdSerializationTest()
        {
            var messageId = JsonRpcId.Null;
            var output = JsonConvert.SerializeObject(messageId, Formatting.Indented);

            output.Should().Be(JsonConvert.SerializeObject(null, Formatting.Indented));
        }

        [Fact]
        public void NoneIdSerializationTest()
        {
            var messageId = JsonRpcId.None;
            var output = JsonConvert.SerializeObject(messageId, Formatting.Indented);

            output.Should().BeEmpty();
        }

        #endregion


        #region Deserialization

        [Theory]
        [MemberData(nameof(JsonRpcIdGenerator.JsonFragments), MemberType = typeof(JsonRpcIdGenerator))]
        public void DeserializationTest(string json, [NotNull] JsonRpcId expected)
        {
            var output = JsonConvert.DeserializeObject<JsonRpcId>(json);

            output.Should().NotBeNull();
            output.Should().Be(expected);
            output.ToString().Should().Be(expected.ToString());
        }

        #endregion


        #region Equality

        [Theory]
        [MemberData(nameof(JsonRpcIdGenerator.ValidStringIds), MemberType = typeof(JsonRpcIdGenerator))]
        [MemberData(nameof(JsonRpcIdGenerator.ValidIntegerIds), MemberType = typeof(JsonRpcIdGenerator))]
        [MemberData(nameof(JsonRpcIdGenerator.ValidFractionIds), MemberType = typeof(JsonRpcIdGenerator))]
        public void ValueEqualityTest(object value)
        {
            var id = JsonRpcId.FromObject(value);

            id.Should().NotBeNull();
            // Reflexive.
            id.Should().Be(id, "RPC ID should be equal to itself.");
            id.Should().Be(id.Value, "RPC ID should be equal to it's value.");
            id.Should().Be(value, "RPC ID should be equal to the original value.");

            // A non-null type RPC ID must not compare equally to null.
            id.Should().NotBe(null);
        }

        [Fact]
        public void NullEqualityTest()
        {
            var id = JsonRpcId.Null;

            id.Should().NotBeNull();
            id.Should().Be(id, "RPC ID should be equal to itself.");
            // Null RPC ID is the only ID type that should be equal to null.
            id.Should().Be(null);

            id.Should().NotBe(string.Empty);
            id.Should().NotBe(0);
        }

        [Fact]
        public void NoneEqualityTest()
        {
            var id = JsonRpcId.None;

            id.Should().NotBeNull();

            id.Should().Be(id, "RPC ID should be equal to itself.");

            // None is only equal to itself.
            id.Should().NotBe(null);
            id.Should().NotBe(string.Empty);
            id.Should().NotBe(0);
        }

        #endregion


        #region Operators

        [Fact]
        public void ImplicitCastOperatorsTest()
        {
            // ReSharper disable once JoinDeclarationAndInitializer
            JsonRpcId id;

            id = (sbyte) -42;
            id.Should().NotBeNull();
            id.Should().Be((sbyte) -42);

            id = (byte) 242;
            id.Should().NotBeNull();
            id.Should().Be((byte) 242);

            id = (short) -42;
            id.Should().NotBeNull();
            id.Should().Be((short) -42);

            id = (ushort) 242;
            id.Should().NotBeNull();
            id.Should().Be((ushort) 242);

            id = -42;
            id.Should().NotBeNull();
            id.Should().Be(-42);

            id = 242u;
            id.Should().NotBeNull();
            id.Should().Be(242u);

            id = -42L;
            id.Should().NotBeNull();
            id.Should().Be(-42L);

            id = 242ul;
            id.Should().NotBeNull();
            id.Should().Be(242ul);

            id = 42.42f;
            id.Should().NotBeNull();
            id.Should().Be(42.42f);

            id = 242.242d;
            id.Should().NotBeNull();
            id.Should().Be(242.242d);

            id = "test-123";
            id.Should().NotBeNull();
            id.Should().Be("test-123");
        }

        #endregion

    }

}
