﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using FluentAssertions;
using FluentAssertions.Json;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using XploRe.Runtime;
using Xunit;


// ReSharper disable AssignNullToNotNullAttribute
// ReSharper disable ObjectCreationAsStatement
// ReSharper disable ReturnValueOfPureMethodIsNotUsed
// ReSharper disable PossibleNullReferenceException

namespace XploRe.Net.JsonRpc.Tests
{

    public class JsonRpcResponseTest
    {

        internal class Generator
        {

            /// <summary>
            ///     Test model used for responses with a specified, simple type.
            /// </summary>
            [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
            private class TestModel
            {

                public int Id
                {
                    [UsedImplicitly]
                    get;
                    set;
                }

                public string Name
                {
                    [UsedImplicitly]
                    get;
                    set;
                }

            }

            /// <summary>
            ///     Yields JSON-RPC response objects with their expected JSON payload.
            /// </summary>
            public static IEnumerable<object[]> JsonRepresentations
            {
                [UsedImplicitly]
                get {
                    // Success response, result=null, id=null.
                    yield return new object[] {
                        JsonRpcResponse.Default,
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": null,
                            ""result"": null
                        }"
                    };

                    // Success response, result=true, id=null.
                    yield return new object[] {
                        JsonRpcResponse.True,
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": null,
                            ""result"": true
                        }"
                    };

                    // Success response, result=false, id=null.
                    yield return new object[] {
                        JsonRpcResponse.False,
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": null,
                            ""result"": false
                        }"
                    };

                    // Success response, result=null, id=42.
                    yield return new object[] {
                        JsonRpcResponse.WithResult((object) null, JsonRpcId.FromNumber(42)),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": 42,
                            ""result"": null
                        }"
                    };

                    // Success response, result=null, id="test".
                    yield return new object[] {
                        JsonRpcResponse.WithResult((object) null, JsonRpcId.FromString("test")),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": ""test"",
                            ""result"": null
                        }"
                    };

                    // Success response, result=-42 (value type), id=null.
                    yield return new object[] {
                        JsonRpcResponse.WithResult(-42),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": null,
                            ""result"": -42
                        }"
                    };

                    // Success response, result=-42 (value type), id=42.
                    yield return new object[] {
                        JsonRpcResponse.WithResult(-42, JsonRpcId.FromNumber(42)),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": 42,
                            ""result"": -42
                        }"
                    };

                    // Success response, result="message", id=null.
                    yield return new object[] {
                        JsonRpcResponse.WithResult("message"),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": null,
                            ""result"": ""message""
                        }"
                    };

                    // Success response, result="message", id=42.
                    yield return new object[] {
                        JsonRpcResponse.WithResult("message", JsonRpcId.FromNumber(42)),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": 42,
                            ""result"": ""message""
                        }"
                    };

                    // Success response, result=TestModel(23, "Test"), id=null.
                    yield return new object[] {
                        JsonRpcResponse.WithResult(new TestModel { Id = 23, Name = "Test" }),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": null,
                            ""result"": { ""id"": 23, ""name"": ""Test"" }
                        }"
                    };

                    // Success response, result=TestModel(23, "Test"), id=42.
                    yield return new object[] {
                        JsonRpcResponse.WithResult(new TestModel { Id = 23, Name = "Test" }, JsonRpcId.FromNumber(42)),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": 42,
                            ""result"": { ""id"": 23, ""name"": ""Test"" }
                        }"
                    };

                    // Error response, data=null, id=null.
                    yield return new object[] {
                        JsonRpcResponse.WithError<object>(1042, "Error message"),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": null,
                            ""error"": { ""code"": 1042, ""message"": ""Error message"" }
                        }"
                    };

                    // Error response, data=null, id=42.
                    yield return new object[] {
                        JsonRpcResponse.WithError<object>(1043, "Error message", JsonRpcId.FromNumber(42)),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": 42,
                            ""error"": { ""code"": 1043, ""message"": ""Error message"" }
                        }"
                    };

                    // Error response, data=null, id="test".
                    yield return new object[] {
                        JsonRpcResponse.WithError<object>(1044, "Error message", JsonRpcId.FromString("test")),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": ""test"",
                            ""error"": { ""code"": 1044, ""message"": ""Error message"" }
                        }"
                    };

                    // Error response, data=23 (value type), id=null.
                    yield return new object[] {
                        JsonRpcResponse.WithError<object>(1045, "Error message", 23, JsonRpcId.Null),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": null,
                            ""error"": { ""code"": 1045, ""message"": ""Error message"", ""data"": 23 }
                        }"
                    };

                    // Error response, data=23 (value type), id=42.
                    yield return new object[] {
                        JsonRpcResponse.WithError<object>(1046, "Error message", 23, JsonRpcId.FromNumber(42)),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": 42,
                            ""error"": { ""code"": 1046, ""message"": ""Error message"", ""data"": 23 }
                        }"
                    };

                    // Error response, data=23 (value type), id=null.
                    yield return new object[] {
                        JsonRpcResponse.WithError<object>(
                            1047, "Error message", new Dictionary<string, object> { { "p0", 23 }, { "info", "Test" } }),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": null,
                            ""error"": { 
                                ""code"": 1047, 
                                ""message"": ""Error message"", 
                                ""data"": { ""p0"": 23, ""info"": ""Test"" } 
                            }
                        }"
                    };

                    // Error response, data=23 (value type), id=42.
                    yield return new object[] {
                        JsonRpcResponse.WithError<object>(
                            1048, "Error message", new Dictionary<string, object> { { "p0", 23 }, { "info", "Test" } },
                            JsonRpcId.FromNumber(42)),
                        @"{
                            ""jsonrpc"": ""2.0"",
                            ""id"": 42,
                            ""error"": { 
                                ""code"": 1048, 
                                ""message"": ""Error message"", 
                                ""data"": { ""p0"": 23, ""info"": ""Test"" } 
                            }
                        }"
                    };
                }
            }

        }


        #region Factories

        [Fact]
        public void CreateWithNoId()
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            Action factorize = () => JsonRpcResponse.WithResult(42, null);

            factorize.ShouldThrowExactly<ArgumentNullException>();
        }

        [Fact]
        public void CreateWithNoneId()
        {
            Action factorize = () => JsonRpcResponse.WithResult(42, JsonRpcId.None);

            factorize.ShouldThrowExactly<ArgumentException>();
        }

        [Fact]
        public void CreateWithBothErrorAndResult()
        {
            Action factorize = () => new JsonRpcResponse(JsonRpcId.Null, new JsonRpcError(24, "Test.", null), 42);

            factorize.ShouldThrowExactly<ArgumentException>();
        }

        #endregion


        #region Serialization

        [Theory]
        [MemberData(nameof(Generator.JsonRepresentations), MemberType = typeof(Generator))]
        public void JsonSerializeTest<T>(JsonRpcResponse<T> rpcResponse, string expectedJson)
        {
            var serializedObjectText = JsonConvert.SerializeObject(rpcResponse);
            serializedObjectText.Should().NotBeNull();
            //-Console.WriteLine(serializedObjectText);
            //-Console.WriteLine(expectedJson);

            // Compare generated JSON.
            var expectedToken = JToken.Parse(expectedJson);
            var actualToken = JToken.Parse(serializedObjectText);
            actualToken.Should().BeEquivalentTo(expectedToken);

            // Object deserialised from JSON should be equivalent to original.
            var deserializedObject = JsonConvert.DeserializeObject<JsonRpcResponse<T>>(serializedObjectText);
            deserializedObject.ShouldBeEquivalentTo(rpcResponse);
        }

        [Theory]
        [MemberData(nameof(Generator.JsonRepresentations), MemberType = typeof(Generator))]
        public void ConvertDeserializeUsingStreamReader<T>(JsonRpcResponse<T> rpcResponse, string expectedJson)
        {
            var reader = new StreamReader(expectedJson.ToMemoryStream(Encoding.UTF8));
            reader.Should().NotBeNull();

            // Object deserialised from JSON should be equivalent to original.
            var deserializedObject = JsonRpcConvert.Deserialize<JsonRpcResponse<T>>(reader);
            deserializedObject.ShouldBeEquivalentTo(rpcResponse);
        }

        #endregion

    }

}
