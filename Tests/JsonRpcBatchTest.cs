﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

// ReSharper disable PossibleNullReferenceException

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FluentAssertions;
using Newtonsoft.Json.Linq;
using Xunit;


namespace XploRe.Net.JsonRpc.Tests
{

    public class JsonRpcBatchTest
    {

        #region Initalizers

        [Fact]
        public void ConstructEmpty()
        {
            // ReSharper disable once CollectionNeverUpdated.Local
            var batch = new JsonRpcBatch<JsonRpcMessage>();

            batch.Should().NotBeNull();
            batch.Should().BeEmpty();
        }

        [Fact]
        public void ConstructWithList()
        {
            Debug.Assert(JsonRpcRequestTest.Generator.JsonRepresentations != null);
            var items = JsonRpcRequestTest.Generator.JsonRepresentations
                                          .Select(list => list[0] as JsonRpcRequest)
                                          .ToList();
            var batch = new JsonRpcBatch<JsonRpcRequest>(items);

            batch.Should().NotBeNull();
            batch.Should().NotBeEmpty();
            batch.Should().Contain(items);
        }

        [Fact]
        public void ConstructWithListThatContainsNull()
        {
            var items = new List<JsonRpcRequest> {
                JsonRpcRequest.NotificationWithMethod("test.1"),
                null,
                JsonRpcRequest.NotificationWithMethod("test.2")
            };

            // ReSharper disable once NotAccessedVariable
            JsonRpcBatch<JsonRpcRequest> batch;
            Action newBatch = () => batch = new JsonRpcBatch<JsonRpcRequest>(items);

            newBatch.ShouldThrowExactly<ArgumentNullException>();
        }

        #endregion


        #region Modifiers

        [Fact]
        public void AddAndInsertItems()
        {
            // ReSharper disable once UseObjectOrCollectionInitializer
            var batch = new JsonRpcBatch<JsonRpcRequest>();
            batch.Should().NotBeNull();

            batch.Add(JsonRpcRequest.NotificationWithMethod("test.1"));
            batch.Add(JsonRpcRequest.NotificationWithMethod("test.2", new[] { 1, 2, 3 }));
            batch.Should().HaveCount(2);

            batch.Insert(0, JsonRpcRequest.NotificationWithMethod("test.1"));
            batch.Insert(0, JsonRpcRequest.NotificationWithMethod("test.2", new[] { 1, 2, 3 }));
            batch.Should().HaveCount(4);
        }

        [Fact]
        public void AddAndInsertNull()
        {
            var batch = new JsonRpcBatch<JsonRpcRequest>();
            batch.Should().NotBeNull();

            Action addNull = () => batch.Add(null);
            addNull.ShouldThrowExactly<ArgumentNullException>();

            Action insertNull = () => batch.Insert(0, null);
            insertNull.ShouldThrowExactly<ArgumentNullException>();
        }

        #endregion


        #region Serialization

        [Fact]
        public void JsonSerializeEmptyRequestBatchTest()
        {
            var batch = new JsonRpcBatch<JsonRpcRequest>();

            var serializedObjectText = JsonRpcConvert.Serialize(batch);
            serializedObjectText.Should().NotBeNull();
            serializedObjectText.Should().BeEmpty();

            // Batch deserialised from JSON should be equivalent to original, as the converter ensures that an empty
            // batch is returned if the decoding result is null (as Json.NET always returns if the JSON string is empty).
            var deserializedBatch = JsonRpcConvert.DeserializeBatch<JsonRpcRequest>(serializedObjectText);
            deserializedBatch.ShouldBeEquivalentTo(batch);
        }

        [Fact]
        public void JsonSerializeEmptyResponseBatchTest()
        {
            var batch = new JsonRpcBatch<JsonRpcResponse>();

            var serializedObjectText = JsonRpcConvert.Serialize(batch);
            serializedObjectText.Should().NotBeNull();
            serializedObjectText.Should().BeEmpty();

            // Batch deserialised from JSON should be equivalent to original, as the converter ensures that an empty
            // batch is returned if the decoding result is null (as Json.NET always returns if the JSON string is empty).
            var deserializedBatch = JsonRpcConvert.DeserializeBatch<JsonRpcResponse>(serializedObjectText);
            deserializedBatch.ShouldBeEquivalentTo(batch);
        }

        [Fact]
        public void JsonSerializeFullRequestBatchTest()
        {
            Debug.Assert(JsonRpcRequestTest.Generator.JsonRepresentations != null);
            var data = JsonRpcRequestTest.Generator.JsonRepresentations.ToList();
            var requests = data.Select(list => list[0] as IJsonRpcRequest);
            var expectedJson = "[" + string.Join(", ", data.Select(list => list[1] as string)) + "]";
            var batch = new JsonRpcBatch<IJsonRpcRequest>(requests.ToList());

            // Serialise batch.
            var serializedObjectText = JsonRpcConvert.Serialize(batch);
            serializedObjectText.Should().NotBeNull();
            //-Console.WriteLine(serializedObjectText);
            //-Console.WriteLine(expectedJson);

            // Compare generated JSON.
            var expectedToken = JToken.Parse(expectedJson);
            var actualToken = JToken.Parse(serializedObjectText);
            JToken.DeepEquals(actualToken, expectedToken).Should().BeTrue();

            // Batch deserialised from JSON should be equivalent to original.
            var deserializedBatch = JsonRpcConvert.DeserializeBatch<JsonRpcRequest>(serializedObjectText);
            deserializedBatch.ShouldBeEquivalentTo(batch);
        }

        [Fact]
        public void JsonSerializeFullResponseBatchTest()
        {
            Debug.Assert(JsonRpcResponseTest.Generator.JsonRepresentations != null);
            // Only use JSON representation tests that can be represented using JsonRpcResponse<dynamic>. This will
            // exclude all test responses with test models as their result type, as those cannot be deserialised in a
            // mixed-type batch and retain their test model type. Hence do not use IJsonResponse here.
            var data = JsonRpcResponseTest.Generator.JsonRepresentations
                                          .Where(list => list[0] is JsonRpcResponse<dynamic>)
                                          .ToList();
            var responses = data.Select(list => list[0] as JsonRpcResponse<dynamic>).ToList();
            var expectedJson = "[" + string.Join(", ", data.Select(list => list[1] as string)) + "]";
            var batch = new JsonRpcBatch<JsonRpcResponse<dynamic>>(responses.ToList());

            responses.Should().NotBeEmpty();

            // Serialise batch.
            var serializedObjectText = JsonRpcConvert.Serialize(batch);
            serializedObjectText.Should().NotBeNull();
            //-Console.WriteLine(serializedObjectText);
            //-Console.WriteLine(expectedJson);

            // Compare generated JSON.
            var expectedToken = JToken.Parse(expectedJson);
            var actualToken = JToken.Parse(serializedObjectText);
            JToken.DeepEquals(actualToken, expectedToken).Should().BeTrue();

            // Batch deserialised from JSON should be equivalent to original.
            var deserializedBatch = JsonRpcConvert.DeserializeBatch<JsonRpcResponse>(serializedObjectText);
            deserializedBatch.ShouldBeEquivalentTo(batch);
        }

        #endregion

    }

}
