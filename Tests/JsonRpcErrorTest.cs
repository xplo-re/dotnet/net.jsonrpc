﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

// ReSharper disable PossibleNullReferenceException

using System.Collections.Generic;
using FluentAssertions;
using FluentAssertions.Json;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;


namespace XploRe.Net.JsonRpc.Tests
{

    public class JsonRpcErrorTest
    {

        private static class Generator
        {

            /// <summary>
            ///     Yields JSON-RPC error objects with their expected JSON payload.
            /// </summary>
            public static IEnumerable<object[]> JsonRepresentations
            {
                [UsedImplicitly]
                get {
                    // Error without additional data.
                    yield return new object[] {
                        new JsonRpcError(42, "The question is not known.", null),
                        @"{
                            ""code"": 42,
                            ""message"": ""The question is not known.""
                        }"
                    };

                    // Error with simple additional data value.
                    yield return new object[] {
                        new JsonRpcError(42, "The question is not known.", -42),
                        @"{
                            ""code"": 42,
                            ""message"": ""The question is not known."",
                            ""data"": -42
                        }"
                    };

                    // Error with simple additional data string.
                    yield return new object[] {
                        new JsonRpcError(42, "The question is not known.", "Run a supercomputer."),
                        @"{
                            ""code"": 42,
                            ""message"": ""The question is not known."",
                            ""data"": ""Run a supercomputer.""
                        }"
                    };

                    // Error with additional data.
                    yield return new object[] {
                        new JsonRpcError(
                            42, "The question is not known.", new Dictionary<string, object> {
                                { "test", "Test string" },
                                { "value", 4 }
                            }),
                        @"{
                            ""code"": 42,
                            ""message"": ""The question is not known."",
                            ""data"": {
                                ""test"": ""Test string"",
                                ""value"": 4
                            }
                        }"
                    };
                }
            }

        }


        #region Serialization

        [Theory]
        [MemberData(nameof(Generator.JsonRepresentations), MemberType = typeof(Generator))]
        public void JsonSerializeTest(JsonRpcError rpcError, string expectedJson)
        {
            var serializedObjectText = JsonConvert.SerializeObject(rpcError);
            serializedObjectText.Should().NotBeNull();

            // Compare generated JSON.
            var expectedToken = JToken.Parse(expectedJson);
            var actualToken = JToken.Parse(serializedObjectText);
            actualToken.Should().BeEquivalentTo(expectedToken);

            // Object deserialised from JSON should be equivalent to original.
            var deserializedObject = JsonConvert.DeserializeObject<JsonRpcError>(serializedObjectText);
            deserializedObject.ShouldBeEquivalentTo(rpcError);
        }

        #endregion

    }

}
