﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Threading.Tasks;
using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc
{

    /// <summary>
    ///     A JSON-RPC client that invokes JSON-RPC requests at a destination and returns the corresponding JSON-RPC
    ///     response.
    /// </summary>
    public interface IJsonRpcClient
    {

        /// <summary>
        ///     Invokes the provided JSON-RPC request and returns the received JSON-RPC response.
        /// </summary>
        /// <param name="request">The <see cref="IJsonRpcRequest" /> instance of the method to invoke.</param>
        /// <typeparam name="T">
        ///     The type of the result of the expected <see cref="JsonRpcResponse{TResult}" />.
        /// </typeparam>
        /// <returns>
        ///     The received <see cref="JsonRpcResponse{TResult}" />. If <paramref name="request" /> is a notification, 
        ///     the returned result is generally <c>null</c>, unless an error occurred.
        /// </returns>
        [NotNull]
        [ItemCanBeNull]
        Task<IJsonRpcResponse<T>> InvokeAsync<T>([NotNull] IJsonRpcRequest request);

        /// <summary>
        ///     Invokes the provided JSON-RPC request and returns the received JSON-RPC response.
        /// </summary>
        /// <param name="request">The <see cref="IJsonRpcRequest" /> instance of the method to invoke.</param>
        /// <param name="settings">
        ///     An optional <see cref="JsonRpcSerializerSettings" /> instance that controls serialisation.
        /// </param>
        /// <typeparam name="T">
        ///     The type of the result of the expected <see cref="JsonRpcResponse{TResult}" />.
        /// </typeparam>
        /// <returns>
        ///     The received <see cref="JsonRpcResponse{TResult}" />. If <paramref name="request" /> is a notification,
        ///     the returned result is generally <c>null</c>, unless an error occurred.
        /// </returns>
        [NotNull]
        [ItemCanBeNull]
        Task<IJsonRpcResponse<T>> InvokeAsync<T>(
            [NotNull] IJsonRpcRequest request,
            [CanBeNull] JsonRpcSerializerSettings settings);

    }

}
