﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc cref="IJsonRpcMessage" />
    /// <summary>
    ///     Base interface for JSON-RPC responses.
    /// </summary>
    public interface IJsonRpcResponse<out TResult> : IJsonRpcMessage, IHasProtocolVersion
    {

        /// <summary>
        ///     Set to the <see cref="JsonRpcError" /> instance if an error occured during the JSON-RPC request. If the 
        ///     request succeeded, this property is <c>null</c>.
        /// </summary>
        [CanBeNull]
        JsonRpcError Error { get; }

        /// <summary>
        ///     The arbitrary JSON-RPC result object. May be <c>null</c>. If an error occurred, this property is always
        ///     <c>null</c>.
        /// </summary>
        [CanBeNull]
        TResult Result { get; }

    }

}
