﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using JetBrains.Annotations;
using Newtonsoft.Json;
using XploRe.Net.JsonRpc.Internal;
using XploRe.Runtime;


namespace XploRe.Net.JsonRpc
{

    /// <summary>
    ///     Provides methods to convert between JSON-RPC types and JSON strings.
    /// </summary>
    public static class JsonRpcConvert
    {

        /// <summary>
        ///     Constructs a JSON-RPC message from a provided JSON string.
        /// </summary>
        /// <param name="message">The JSON string to deserialise.</param>
        /// <typeparam name="T">The type of the JSON-RPC message to construct.</typeparam>
        /// <returns>A new JSON-RPC message instance constructed from the provided JSON string.</returns>
        [Pure]
        [CanBeNull]
        public static T Deserialize<T>([NotNull] string message) where T : JsonRpcMessage
            => Deserialize<T>(message, null);

        /// <summary>
        ///     Constructs a JSON-RPC message from a provided JSON string.
        /// </summary>
        /// <param name="message">The JSON string to deserialise.</param>
        /// <param name="settings">
        ///     An optional <see cref="JsonRpcSerializerSettings" /> instance to control JSON-RPC message
        ///     deserialisation.
        /// </param>
        /// <typeparam name="T">The type of the JSON-RPC message to construct.</typeparam>
        /// <returns>A new JSON-RPC message instance constructed from the provided JSON string.</returns>
        [Pure]
        [CanBeNull]
        public static T Deserialize<T>(
            [NotNull] string message,
            [CanBeNull] JsonRpcSerializerSettings settings)
            where T : JsonRpcMessage
        {
            if (message == null) {
                throw new ArgumentNullException(nameof(message));
            }

            var result = JsonConvert.DeserializeObject<T>(message, settings);

            if (result != null && settings != null) {
                EnforceSerializerSettings(result, settings);
            }

            return result;
        }

        /// <summary>
        ///     Constructs a JSON-RPC message from a provided stream reader.
        /// </summary>
        /// <param name="reader">The <see cref="StreamReader" /> instance to read JSON from.</param>
        /// <typeparam name="T">The type of the JSON-RPC message to construct.</typeparam>
        /// <returns>A new JSON-RPC message instance constructed from the provided JSON string.</returns>
        [Pure]
        [CanBeNull]
        public static T Deserialize<T>([NotNull] StreamReader reader) where T : JsonRpcMessage
            => Deserialize<T>(reader, null);

        /// <summary>
        ///     Constructs a JSON-RPC message from a provided stream reader.
        /// </summary>
        /// <param name="reader">The <see cref="StreamReader" /> instance to read JSON from.</param>
        /// <param name="settings">
        ///     An optional <see cref="JsonRpcSerializerSettings" /> instance to control JSON-RPC message
        ///     deserialisation.
        /// </param>
        /// <typeparam name="T">The type of the JSON-RPC message to construct.</typeparam>
        /// <returns>A new JSON-RPC message instance constructed from the provided JSON string.</returns>
        [Pure]
        [CanBeNull]
        public static T Deserialize<T>(
            [NotNull] StreamReader reader,
            [CanBeNull] JsonRpcSerializerSettings settings)
            where T : JsonRpcMessage
        {
            if (reader == null) {
                throw new ArgumentNullException(nameof(reader));
            }

            var serializer = JsonSerializer.CreateDefault(settings);

            if (serializer == null) {
                throw new JsonRpcException("Failed to initialise JSON serialiser.");
            }

            T result;

            using (var jsonTextReader = new JsonTextReader(reader)) {
                result = serializer.Deserialize<T>(jsonTextReader);
            }

            if (result != null && settings != null) {
                EnforceSerializerSettings(result, settings);
            }

            return result;
        }

        /// <summary>
        ///     Constructs a JSON-RPC message batch from a provided JSON string.
        /// </summary>
        /// <param name="messages">The JSON string to deserialise.</param>
        /// <typeparam name="T">The type of the JSON-RPC messages of the batch to construct.</typeparam>
        /// <returns>A new JSON-RPC message batch instance constructed from the provided JSON string.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcBatch<T> DeserializeBatch<T>([NotNull] string messages) where T : JsonRpcMessage
            => DeserializeBatch<T>(messages, null);

        /// <summary>
        ///     Constructs a JSON-RPC message batch from a provided JSON string.
        /// </summary>
        /// <param name="messages">The JSON string to deserialise.</param>
        /// <param name="settings">
        ///     An optional <see cref="JsonRpcSerializerSettings" /> instance to control JSON-RPC message batch
        ///     deserialisation.
        /// </param>
        /// <typeparam name="T">The type of the JSON-RPC messages of the batch to construct.</typeparam>
        /// <returns>A new JSON-RPC message batch instance constructed from the provided JSON string.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcBatch<T> DeserializeBatch<T>(
            [NotNull] string messages,
            [CanBeNull] JsonRpcSerializerSettings settings)
            where T : JsonRpcMessage
        {
            if (messages == null) {
                throw new ArgumentNullException(nameof(messages));
            }

            if (settings == null) {
                settings = new JsonRpcSerializerSettings();
            }

            if (settings.Converters == null) {
                settings.Converters = new List<JsonConverter>();
            }

            settings.Converters.Insert(0, new JsonRpcBatchConverter<T>());

            var batch = JsonConvert.DeserializeObject<JsonRpcBatch<T>>(messages, settings);

            if (batch == null) {
                // JSON message was empty, return empty batch object.
                return new JsonRpcBatch<T>();
            }

            foreach (var message in batch) {
                Debug.Assert(message != null, $"{nameof(message)} != null");
                EnforceSerializerSettings(message, settings);
            }

            return batch;
        }

        /// <summary>
        ///     Serialises a JSON-RPC message to JSON.
        /// </summary>
        /// <param name="message">The <see cref="IJsonRpcMessage" /> to serialise.</param>
        /// <typeparam name="T">The type of the message.</typeparam>
        /// <returns>A <see cref="string" /> with the serialised message.</returns>
        [Pure]
        [NotNull]
        public static string Serialize<T>([NotNull] T message) where T : IJsonRpcMessage
            => Serialize(message, null);

        /// <summary>
        ///     Serialises a JSON-RPC message to JSON.
        /// </summary>
        /// <param name="message">The <see cref="IJsonRpcMessage" /> to serialise.</param>
        /// <param name="settings">
        ///     An optional <see cref="JsonRpcSerializerSettings" /> instance to control JSON-RPC message serialisation.
        /// </param>
        /// <typeparam name="T">The type of the message.</typeparam>
        /// <returns>A <see cref="string" /> with the serialised message.</returns>
        [Pure]
        [NotNull]
        public static string Serialize<T>(
            [NotNull] T message,
            [CanBeNull] JsonRpcSerializerSettings settings)
            where T : IJsonRpcMessage
        {
            if (message == null) {
                throw new ArgumentNullException(nameof(message));
            }

            if (settings != null) {
                EnforceSerializerSettings(message, settings);
            }

            return JsonConvert.SerializeObject(message, settings)
                   ?? throw new RuntimeInconsistencyException("JSON serializer returned null.");
        }

        /// <summary>
        ///     Serialises a JSON-RPC batch to JSON.
        /// </summary>
        /// <param name="batch">The <see cref="JsonRpcBatch{T}" /> to serialise.</param>
        /// <typeparam name="T">The type of the messages in the batch.</typeparam>
        /// <returns>A <see cref="string" /> with the serialised batch.</returns>
        [Pure]
        [NotNull]
        public static string Serialize<T>([NotNull] JsonRpcBatch<T> batch) where T : IJsonRpcMessage
            => Serialize(batch, null);

        /// <summary>
        ///     Serialises a JSON-RPC batch to JSON.
        /// </summary>
        /// <param name="batch">The <see cref="JsonRpcBatch{T}" /> to serialise.</param>
        /// <param name="settings">
        ///     An optional <see cref="JsonRpcSerializerSettings" /> instance to control JSON-RPC message batch
        ///     serialisation.
        /// </param>
        /// <typeparam name="T">The type of the messages in the batch.</typeparam>
        /// <returns>A <see cref="string" /> with the serialised batch.</returns>
        [Pure]
        [NotNull]
        public static string Serialize<T>(
            [NotNull] JsonRpcBatch<T> batch,
            [CanBeNull] JsonRpcSerializerSettings settings)
            where T : IJsonRpcMessage
        {
            if (batch == null) {
                throw new ArgumentNullException(nameof(batch));
            }

            if (settings == null) {
                settings = new JsonRpcSerializerSettings();
            }

            if (settings.Converters == null) {
                settings.Converters = new List<JsonConverter>();
            }

            settings.Converters.Insert(0, new JsonRpcBatchConverter<T>());

            foreach (var message in batch) {
                Debug.Assert(message != null, $"{nameof(message)} != null");
                EnforceSerializerSettings(message, settings);
            }

            return JsonConvert.SerializeObject(batch, settings)
                   ?? throw new RuntimeInconsistencyException("JSON serializer returned null.");
        }

        /// <summary>
        ///     Enfores extended JSON-RPC serialiser settings as the Json.NET serialiser settings are not extendable.
        /// </summary>
        /// <param name="message">The <see cref="IJsonRpcMessage" /> instance to enforce settings on.</param>
        /// <param name="settings">The <see cref="JsonSerializerSettings" /> instance to enforce.</param>
        private static void EnforceSerializerSettings(
            [NotNull] IJsonRpcMessage message,
            [NotNull] JsonRpcSerializerSettings settings)
        {
            if (settings.ProtocolCompatiblityMode == JsonRpcProtocolCompatiblityMode.Strict) {
                // Enforce matching version.
                if (message is IHasProtocolVersion versionedMessage &&
                    versionedMessage.ProtocolVersion != settings.ProtocolVersion) {
                    throw new JsonRpcProtocolException(
                        "JSON-RPC version mismatch. Expected version '{0}', message version is '{1}'.".FormatWith(
                            settings.ProtocolVersion,
                            versionedMessage.ProtocolVersion
                        ),
                        message.Id
                    );
                }
            }
        }

    }

}
