﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Newtonsoft.Json;
using XploRe.Net.JsonRpc.Internal;
using XploRe.Runtime;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc />
    /// <summary>
    ///     A JSON-RPC message ID. Per JSON-RPC RFC, the ID can be of any scalar JSON type. 
    /// </summary>
    [JsonConverter(typeof(JsonRpcIdJsonConverter))]
    public abstract class JsonRpcId : IEquatable<JsonRpcId>
    {

        /// <summary>
        ///     The JSON-RPC message ID instance that represents an undefined message ID that is used by notifications
        ///     and thus omitted in the actual payload.
        /// </summary>
        [NotNull]
        public static readonly JsonRpcId None = new NoneJsonRpcId();

        /// <summary>
        ///     The JSON-RPC message ID instance that represents the <c>null</c> message ID. Although the standard
        ///     allows <c>null</c> message IDs, their use is discouraged.
        /// </summary>
        [NotNull]
        public static readonly JsonRpcId Null = new NullJsonRpcId();

        /// <summary>
        ///     The type of the JSON-RPC message ID.
        /// </summary>
        public abstract JsonRpcIdType Type { get; }

        /// <summary>
        ///     The value of the JSON-RPC message ID.
        /// </summary>
        [CanBeNull]
        public abstract object Value { get; }

        /// <summary>
        ///     Instances are created using factory methods to return instanced of type-specific classes. Use the 
        ///     corresponding factory methods to create new <see cref="JsonRpcId" /> instances.
        /// </summary>
        /// <seealso cref="FromNumber(double)" />
        /// <seealso cref="FromNumber(long)"/>
        /// <seealso cref="FromObject" />
        /// <seealso cref="FromString" />
        internal JsonRpcId()
        {
        }

        /// <summary>
        ///     Returns a string representation of the JSON-RPC message ID.
        /// </summary>
        /// <returns>String representation of the JSON-RPC message ID.</returns>
        [NotNull]
        public abstract override string ToString();


        #region Factories

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a string value.
        /// </summary>
        /// <param name="id">ID to initialise new <see cref="JsonRpcId" /> instance with.</param>
        /// <returns>New <see cref="JsonRpcId" /> instance.</returns>
        [NotNull]
        public static JsonRpcId FromString([NotNull] string id)
        {
            if (id == null) {
                throw new ArgumentNullException(nameof(id));
            }

            return new StringJsonRpcId(id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from an integer value.
        /// </summary>
        /// <param name="id">ID to initialise new <see cref="JsonRpcId" /> instance with.</param>
        /// <returns>New <see cref="JsonRpcId" /> instance.</returns>
        [NotNull]
        public static JsonRpcId FromNumber(long id)
        {
            return new NumberJsonRpcId(id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a fractional number value.
        /// </summary>
        /// <param name="id">ID to initialise new <see cref="JsonRpcId" /> instance with.</param>
        /// <returns>New <see cref="JsonRpcId" /> instance.</returns>
        [NotNull]
        public static JsonRpcId FromNumber(double id)
        {
            return new NumberJsonRpcId(id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from any compatible type.
        /// </summary>
        /// <param name="id">
        ///     ID value to initialise instance with. The following types are supported:
        ///     <ul>
        ///     <li><see cref="string" /> objects,</li>
        ///     <li>integer types (<see cref="sbyte" />, <see cref="byte" />, <see cref="short" />, <see cref="ushort" />,
        ///         <see cref="int" />, <see cref="uint" />, <see cref="long" />, <see cref="ulong" />),</li>
        ///     <li>floating-point types (<see cref="float" />, <see cref="double" />),</li>
        ///     <li>the <c>null</c> reference.</li>
        ///     </ul>
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance on success, otherwise an exception is thrown.</returns>
        /// <exception cref="ArgumentException">
        ///     Provided argument <paramref name="id" /> is of an unsupported type.
        /// </exception>
        [NotNull]
        public static JsonRpcId FromObject(object id)
        {
            switch (id) {
            case null:
                return Null;

            case string value:
                return FromString(value);

            case sbyte value:
                return FromNumber(value);

            case byte value:
                return FromNumber(value);

            case short value:
                return FromNumber(value);

            case ushort value:
                return FromNumber(value);

            case int value:
                return FromNumber(value);

            case uint value:
                return FromNumber(value);

            case long value:
                return FromNumber(value);

            case ulong value:
                return FromNumber(value);

            case float value:
                return FromNumber(value);

            case double value:
                return FromNumber(value);

            default:
                throw new ArgumentException(
                    $"Argument type {id.GetType()} is not a valid JSON-RPC message ID type.", nameof(id)
                );
            }
        }

        /// <summary>
        ///     Synchronisation lock object for sequential IDs.
        /// </summary>
        [NotNull]
        private static readonly object LastSequenceIdLock = new object();

        /// <summary>
        ///     The per-process last assigned sequence ID. Requires the use of a synchronisation lock to access for 
        ///     thread-safety.
        /// </summary>
        private static long _lastSequenceId;

        /// <summary>
        ///     Returns a new per-process unique sequential numeric <see cref="JsonRpcId" />.
        /// </summary>
        /// <returns>New per-proces unique sequential numeric <see cref="JsonRpcId" /> instance.</returns>
        [NotNull]
        public static JsonRpcId NextSequential()
        {
            long id;

            lock (LastSequenceIdLock) {
                id = ++_lastSequenceId;
            }

            return FromNumber(id);
        }

        /// <summary>
        ///     Returns a randomly generated JSON-RPC message ID.
        /// </summary>
        /// <returns>Randomly generated JSON-RPC message ID.</returns>
        [NotNull]
        public static JsonRpcId Random()
        {
            var guid = Guid.NewGuid().ToString();

            if (guid == null) {
                throw new RuntimeInconsistencyException(
                    "String representation of GUID returned from {0}.{1}() is null.".FormatWith(
                        typeof(Guid), nameof(Guid.NewGuid)
                    )
                );
            }

            return FromString(guid);
        }

        #endregion


        #region Equality

        /// <inheritdoc />
        public bool Equals(JsonRpcId other)
        {
            if (ReferenceEquals(other, null)) {
                return false;
            }

            if (ReferenceEquals(other, this)) {
                return true;
            }

            if (Type != other.Type) {
                return false;
            }

            return Value == other.Value;
        }

        /// <inheritdoc />
        public abstract override bool Equals(object obj);

        /// <summary>
        ///     Returns the hash code of the <see cref="Value" /> property.
        /// </summary>
        /// <returns>The hash code of the <see cref="Value" /> property.</returns>
        public abstract override int GetHashCode();

        /// <summary>
        ///     Tests two <see cref="JsonRpcId" /> instances for equality.
        /// </summary>
        /// <param name="lhs">First <see cref="JsonRpcId" /> instance to compare to second.</param>
        /// <param name="rhs">Second <see cref="JsonRpcId" /> instance to compare to first.</param>
        /// <returns><c>true</c> if both instances are considered equal, otherwise <c>false</c>.</returns>
        public static bool operator ==(JsonRpcId lhs, JsonRpcId rhs)
        {
            if (ReferenceEquals(lhs, null)) {
                if (ReferenceEquals(rhs, null)) {
                    return true;
                }

                return false;
            }

            return lhs.Equals(rhs);
        }

        /// <summary>
        ///     Tests two <see cref="JsonRpcId" /> instances for inequality.
        /// </summary>
        /// <param name="lhs">First <see cref="JsonRpcId" /> instance to compare to second.</param>
        /// <param name="rhs">Second <see cref="JsonRpcId" /> instance to compare to first.</param>
        /// <returns><c>true</c> if both instances are not considered equal, otherwise <c>false</c>.</returns>
        public static bool operator !=(JsonRpcId lhs, JsonRpcId rhs)
        {
            return !(lhs == rhs);
        }

        #endregion


        #region Casts

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a <see cref="sbyte" /> value.
        /// </summary>
        /// <param name="value">
        ///     The <see cref="sbyte" /> value to create a new <see cref="JsonRpcId" /> instance from.
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance for the given value.</returns>
        [NotNull]
        public static implicit operator JsonRpcId(sbyte value)
        {
            return FromNumber(value);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a <see cref="byte" /> value.
        /// </summary>
        /// <param name="value">
        ///     The <see cref="byte" /> value to create a new <see cref="JsonRpcId" /> instance from.
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance for the given value.</returns>
        [NotNull]
        public static implicit operator JsonRpcId(byte value)
        {
            return FromNumber(value);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a <see cref="short" /> value.
        /// </summary>
        /// <param name="value">
        ///     The <see cref="short" /> value to create a new <see cref="JsonRpcId" /> instance from.
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance for the given value.</returns>
        [NotNull]
        public static implicit operator JsonRpcId(short value)
        {
            return FromNumber(value);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a <see cref="ushort" /> value.
        /// </summary>
        /// <param name="value">
        ///     The <see cref="ushort" /> value to create a new <see cref="JsonRpcId" /> instance from.
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance for the given value.</returns>
        [NotNull]
        public static implicit operator JsonRpcId(ushort value)
        {
            return FromNumber(value);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a <see cref="int" /> value.
        /// </summary>
        /// <param name="value">
        ///     The <see cref="int" /> value to create a new <see cref="JsonRpcId" /> instance from.
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance for the given value.</returns>
        [NotNull]
        public static implicit operator JsonRpcId(int value)
        {
            return FromNumber(value);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a <see cref="uint" /> value.
        /// </summary>
        /// <param name="value">
        ///     The <see cref="uint" /> value to create a new <see cref="JsonRpcId" /> instance from.
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance for the given value.</returns>
        [NotNull]
        public static implicit operator JsonRpcId(uint value)
        {
            return FromNumber(value);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a <see cref="long" /> value.
        /// </summary>
        /// <param name="value">
        ///     The <see cref="long" /> value to create a new <see cref="JsonRpcId" /> instance from.
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance for the given value.</returns>
        [NotNull]
        public static implicit operator JsonRpcId(long value)
        {
            return FromNumber(value);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a <see cref="ulong" /> value.
        /// </summary>
        /// <param name="value">
        ///     The <see cref="ulong" /> value to create a new <see cref="JsonRpcId" /> instance from.
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance for the given value.</returns>
        [NotNull]
        public static implicit operator JsonRpcId(ulong value)
        {
            return FromNumber(value);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a <see cref="float" /> value.
        /// </summary>
        /// <param name="value">
        ///     The <see cref="float" /> value to create a new <see cref="JsonRpcId" /> instance from.
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance for the given value.</returns>
        [NotNull]
        public static implicit operator JsonRpcId(float value)
        {
            return FromNumber(value);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a <see cref="double" /> value.
        /// </summary>
        /// <param name="value">
        ///     The <see cref="double" /> value to create a new <see cref="JsonRpcId" /> instance from.
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance for the given value.</returns>
        [NotNull]
        public static implicit operator JsonRpcId(double value)
        {
            return FromNumber(value);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcId" /> instance from a <see cref="string" /> value.
        /// </summary>
        /// <param name="value">
        ///     The <see cref="string" /> value to create a new <see cref="JsonRpcId" /> instance from.
        /// </param>
        /// <returns>New <see cref="JsonRpcId" /> instance for the given value.</returns>
        [NotNull]
        public static implicit operator JsonRpcId([NotNull] string value)
        {
            return FromString(value);
        }

        #endregion

    }

}
