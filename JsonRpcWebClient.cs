﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using JetBrains.Annotations;
using XploRe.Runtime;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc />
    /// <summary>
    ///     Simple JSON-RPC web client to run requests against a service endpoint. The protocol of the service endpoint
    ///     must be a registered <see cref="P:XploRe.Net.JsonRpc.JsonRpcWebClient.WebRequest" /> prefix. 
    /// </summary>
    public class JsonRpcWebClient : IJsonRpcClient
    {

        private WebRequest _webRequest;


        #region Properties 

        /// <summary>
        ///     Configured service endpoint <see cref="Uri" />.
        /// </summary>
        [NotNull]
        public Uri ServiceEndpoint { get; }

        /// <summary>
        ///     Collection of headers used when performing the request.
        /// </summary>
        [NotNull]
        public WebHeaderCollection Headers
        {
            get => WebRequest.Headers;
            set => WebRequest.Headers = value;
        }

        /// <summary>
        ///     Cached <see cref="WebRequest" /> instance for the configured service endpoint that is automatically
        ///     initialised on first access.
        /// </summary>
        /// <exception cref="JsonRpcWebException">
        ///     Failed to initialise <see cref="WebRequest" /> instance with the configured <see cref="ServiceEndpoint" />.
        /// </exception>
        [NotNull]
        protected WebRequest WebRequest
        {
            get {
                if (_webRequest == null) {
                    _webRequest = WebRequest.Create(ServiceEndpoint);

                    if (_webRequest == null) {
                        throw new JsonRpcWebException(
                            $"Failed to initialise web request with service endpoint '{ServiceEndpoint}'."
                        );
                    }

                    _webRequest.Method = "POST";
                    _webRequest.ContentType = "application/json";
                }

                return _webRequest;
            }
        }

        #endregion


        /// <summary>
        ///     Initialises a new <see cref="JsonRpcWebClient" /> instance with a given service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The service endpoint <see cref="Uri" /> used for requests.</param>
        public JsonRpcWebClient([NotNull] Uri serviceEndpoint)
        {
            ServiceEndpoint = serviceEndpoint;
        }


        #region IJsonRpcClient

        /// <inheritdoc />
        public virtual async Task<IJsonRpcResponse<T>> InvokeAsync<T>(IJsonRpcRequest request)
            => await InvokeAsync<T>(request, null);

        /// <inheritdoc />
        public virtual async Task<IJsonRpcResponse<T>> InvokeAsync<T>(
            IJsonRpcRequest request,
            JsonRpcSerializerSettings settings)
        {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            var message = JsonRpcConvert.Serialize(request);
            var response = await SendRequest(message);

            return JsonRpcConvert.Deserialize<JsonRpcResponse<T>>(response, settings);
        }

        #endregion


        [NotNull]
        [ItemNotNull]
        private async Task<string> SendRequest([NotNull] string message)
        {
            var getRequestStreamTask = WebRequest.GetRequestStreamAsync();
            if (getRequestStreamTask == null) {
                throw RuntimeInconsistencyException.FromUnexpectedNull(() => WebRequest.GetRequestStreamAsync());
            }

            using (var requestStream = await getRequestStreamTask) {
                using (var writer = new StreamWriter(requestStream)) {
                    var writeTask = writer.WriteAsync(message);
                    if (writeTask == null) {
                        // ReSharper disable once AccessToDisposedClosure
                        throw RuntimeInconsistencyException.FromUnexpectedNull(() => writer.WriteAsync(message));
                    }

                    await writeTask;
                }
            }

            var getResponseTask = WebRequest.GetResponseAsync();
            if (getResponseTask == null) {
                throw RuntimeInconsistencyException.FromUnexpectedNull(() => WebRequest.GetResponseAsync());
            }

            using (var response = await getResponseTask) {
                if (response == null) {
                    throw new JsonRpcWebException("Response from web request is null.");
                }

                using (var responseStream = response.GetResponseStream()) {
                    using (var reader = new StreamReader(responseStream)) {
                        var readerTask = reader.ReadToEndAsync();
                        if (readerTask == null) {
                            // ReSharper disable once AccessToDisposedClosure
                            throw RuntimeInconsistencyException.FromUnexpectedNull(() => reader.ReadToEndAsync());
                        }

                        var result = await readerTask;

                        if (result == null) {
                            throw new JsonRpcWebException("Response stream result is null.");
                        }

                        return result;
                    }
                }
            }
        }

    }

}
