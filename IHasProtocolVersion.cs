﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

namespace XploRe.Net.JsonRpc
{

    /// <summary>
    ///     Provides an embedded protocol version.
    /// </summary>
    public interface IHasProtocolVersion
    {

        /// <summary>
        ///     The protocol version that is embedded in the message.
        /// </summary>
        string ProtocolVersion { get; }

    }

}
