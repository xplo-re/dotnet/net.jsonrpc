﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc
{

    /// <summary>
    ///     Provides status methods for <see cref="JsonRpcResponse{TResult}" /> instances.
    /// </summary>
    public static class JsonRpcResponseExtensions
    {

        /// <summary>
        ///     Determines whether the response represents an error. 
        /// </summary>
        /// <param name="response">This <see cref="IJsonRpcResponse{TResult}" /> instance.</param>
        /// <returns><c>true</c>, If the response represents an error, otherwise <c>false</c>.</returns>
        [Pure]
        public static bool IsError<T>([NotNull] this IJsonRpcResponse<T> response)
        {
            return response.Error != null;
        }

    }

}
