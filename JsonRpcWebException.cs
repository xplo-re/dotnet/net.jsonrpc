﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Runtime.InteropServices;
using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc />
    /// <summary>
    ///     Exception thrown by JSON-RPC web clients and servers.
    /// </summary>
    [ComVisible(true)]
    public class JsonRpcWebException : JsonRpcException
    {

        private const string DefaultErrorMessage = "JSON-RPC web error.";

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcWebException" /> instance with the default error message.
        /// </summary>
        public JsonRpcWebException()
            : base(DefaultErrorMessage)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcWebException" /> instance with the default error message and a 
        ///     reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcWebException([CanBeNull] Exception innerException)
            : base(DefaultErrorMessage, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcWebException" /> instance with a specified error message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public JsonRpcWebException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcWebException" /> instance with the default error message and 
        ///     the ID of the related JSON-RPC message.
        /// </summary>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        public JsonRpcWebException([CanBeNull] JsonRpcId id)
            : this(DefaultErrorMessage, id)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcWebException" /> instance with a specified error message and a 
        ///     reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcWebException(string message, [CanBeNull] Exception innerException)
            : base(message, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcWebException" /> instance with the default error message, the 
        ///     ID of the related JSON-RPC message and a reference to the inner exception that is the cause of this 
        ///     exception.
        /// </summary>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcWebException([CanBeNull] JsonRpcId id, [CanBeNull] Exception innerException)
            : this(DefaultErrorMessage, id, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcWebException" /> instance with a specified error message and 
        ///     the ID of the related JSON-RPC message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        public JsonRpcWebException(string message, [CanBeNull] JsonRpcId id)
            : base(message, id)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcWebException" /> instance with a specified error message, the 
        ///     ID of the related JSON-RPC message and a reference to the inner exception that is the cause of this 
        ///     exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcWebException(string message, [CanBeNull] JsonRpcId id, [CanBeNull] Exception innerException)
            : base(message, id, innerException)
        {
        }

    }

}
