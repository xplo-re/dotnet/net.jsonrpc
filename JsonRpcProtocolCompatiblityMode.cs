﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

namespace XploRe.Net.JsonRpc
{

    /// <summary>
    ///     Controls the treatment of JSON-RPC protocol versions in JSON-RPC messages.
    /// </summary>
    public enum JsonRpcProtocolCompatiblityMode
    {

        /// <summary>
        ///     No JSON-RPC protocol version is enforced. Received messages are deserialised on a best-effort basis.
        /// </summary>
        Relaxed = 0,

        /// <summary>
        ///     Enfores the configured JSON-RPC protocol version. If the JSON-RPC protocol version of a message does not
        ///     match the configured protocol version or is missing, an exception is thrown when deserialising the 
        ///     message.
        /// </summary>
        Strict = 1

    }

}
