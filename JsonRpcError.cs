﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using JetBrains.Annotations;
using Newtonsoft.Json;
using XploRe.Json;


namespace XploRe.Net.JsonRpc
{

    /// <summary>
    ///     The error object of a JSON-RPC response if the request failed.
    /// </summary>
    /// <seealso href="http://www.jsonrpc.org/specification#error_object">JSON-RPC 2.0 Specification: Error Object</seealso>
    [JsonObject(MemberSerialization.OptIn)]
    public class JsonRpcError
    {

        #region JSON Property Names 

        /// <summary>
        ///     JSON-RPC payload property name of the <see cref="Code" /> property.
        /// </summary>
        public const string CodeJsonPropertyName = "code";

        /// <summary>
        ///     JSON-RPC payload property name of the <see cref="Data" /> property.
        /// </summary>
        public const string DataJsonPropertyName = "data";

        /// <summary>
        ///     JSON-RPC payload property name of the <see cref="Message" /> property.
        /// </summary>
        public const string MessageJsonPropertyName = "message";

        #endregion


        #region Properties

        /// <summary>
        ///     Error code from the JSON-RPC server that indicates the type of the error.
        /// </summary>
        [JsonProperty(CodeJsonPropertyName)]
        public int Code { get; }

        /// <summary>
        ///     An optional primitive or structured value that contains additional information about the rror. If no
        ///     further data is given, this property is <c>null</c>.
        /// </summary>
        [CanBeNull]
        [JsonProperty(DataJsonPropertyName, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(CollectionConverter))]
        public object Data { get; }

        /// <summary>
        ///     A short description of the error.
        /// </summary>
        [NotNull]
        [JsonProperty(MessageJsonPropertyName)]
        public string Message { get; }

        #endregion


        /// <summary>
        ///     Initialises a new JSON-RPC error response object.
        /// </summary>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <param name="data">
        ///     An optional primitive or structured value with additional information about the error.
        /// </param>
        public JsonRpcError(
            int code,
            [NotNull] string message,
            [CanBeNull] object data)
        {
            Code = code;
            Message = message;
            Data = data;
        }

    }

}
