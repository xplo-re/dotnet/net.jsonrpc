﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Runtime.InteropServices;
using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc />
    /// <summary>
    ///     Exception thrown on JSON-RPC protocol errors.
    /// </summary>
    [ComVisible(true)]
    public class JsonRpcProtocolException : JsonRpcException
    {

        private const string DefaultErrorMessage = "JSON-RPC protocol error.";

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcProtocolException" /> instance with the default error message.
        /// </summary>
        public JsonRpcProtocolException()
            : base(DefaultErrorMessage)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcProtocolException" /> instance with the default error message and a 
        ///     reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcProtocolException([CanBeNull] Exception innerException)
            : base(DefaultErrorMessage, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcProtocolException" /> instance with a specified error message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public JsonRpcProtocolException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcProtocolException" /> instance with the default error message and 
        ///     the ID of the related JSON-RPC message.
        /// </summary>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        public JsonRpcProtocolException([CanBeNull] JsonRpcId id)
            : this(DefaultErrorMessage, id)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcProtocolException" /> instance with a specified error message and a 
        ///     reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcProtocolException(string message, [CanBeNull] Exception innerException)
            : base(message, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcProtocolException" /> instance with the default error message, the 
        ///     ID of the related JSON-RPC message and a reference to the inner exception that is the cause of this 
        ///     exception.
        /// </summary>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcProtocolException([CanBeNull] JsonRpcId id, [CanBeNull] Exception innerException)
            : this(DefaultErrorMessage, id, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcProtocolException" /> instance with a specified error message and 
        ///     the ID of the related JSON-RPC message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        public JsonRpcProtocolException(string message, [CanBeNull] JsonRpcId id)
            : base(message, id)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="JsonRpcProtocolException" /> instance with a specified error message, the 
        ///     ID of the related JSON-RPC message and a reference to the inner exception that is the cause of this 
        ///     exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="id">The <see cref="JsonRpcId" /> instance of the message that caused the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JsonRpcProtocolException(string message, [CanBeNull] JsonRpcId id, [CanBeNull] Exception innerException)
            : base(message, id, innerException)
        {
        }

    }

}
