﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Newtonsoft.Json;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc />
    /// <summary>
    ///     Base class for JSON-RPC messages.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public abstract class JsonRpcMessage : IJsonRpcMessage
    {

        #region JSON Property Names 

        /// <summary>
        ///     JSON-RPC payload property name of the <see cref="Id" /> property.
        /// </summary>
        public const string IdJsonPropertyName = "id";

        #endregion


        #region Properties

        /// <inheritdoc />
        [JsonProperty(IdJsonPropertyName, Order = -2 /* Default is -1. */)]
        public JsonRpcId Id { get; }

        #endregion


        /// <summary>
        ///     Initialises a new JSON-RPC message instance with a given ID.
        /// </summary>
        /// <param name="id">The <see cref="JsonRpcId" /> instance to assign to the message.</param>
        protected JsonRpcMessage([NotNull] JsonRpcId id)
        {
            if (id == null) {
                throw new ArgumentNullException(nameof(id));
            }

            Id = id;
        }

        /// <summary>
        ///     Controls serialisation of the <see cref="Id" /> property. The ID is omitted in the payload for
        ///     notification messages.
        /// </summary>
        [Pure]
        public bool ShouldSerializeId() => Id.Type != JsonRpcIdType.None;

    }

}
