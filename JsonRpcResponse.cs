﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using XploRe.Net.JsonRpc.Internal;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc cref="JsonRpcMessage" />
    /// <summary>
    ///     a JSON-RPC server response which may either indicate success, with an arbitrary response object, or an error
    ///     with a corresponding <see cref="T:XploRe.Net.JsonRpc.JsonRpcError" /> instance describing the error. 
    /// </summary>
    /// <typeparam name="TResult">
    ///     The expected type of the response result on success. If the result returned by the server cannot be
    ///     converted to the requested type, a <see cref="T:Newtonsoft.Json.JsonSerializationException" /> is thrown.
    ///     <para>
    ///     The conversion is directly performed during JSON deserialisation and respects all Newtonsoft.Json attributes
    ///     and serialisation settings.
    ///     </para>
    /// </typeparam>
    /// <remarks>
    ///     The <see cref="P:XploRe.Net.JsonRpc.JsonRpcResponse`1.Error" /> and 
    ///     <see cref="P:XploRe.Net.JsonRpc.JsonRpcResponse`1.Result" /> properties are mutually exclusive. If the 
    ///     response indicates an error, the <see cref="P:XploRe.Net.JsonRpc.JsonRpcResponse`1.Error" /> property is set
    ///     to a <see cref="T:XploRe.Net.JsonRpc.JsonRpcError" /> instance with details on the error. Otherwise, the 
    ///     <see cref="P:XploRe.Net.JsonRpc.JsonRpcResponse`1.Error" /> property is <c>null</c> and omitted in the 
    ///     payload; the <see cref="P:XploRe.Net.JsonRpc.JsonRpcResponse`1.Result" /> property must exist in the payload
    ///     but may be <c>null</c>.
    /// </remarks>
    /// <seealso href="http://www.jsonrpc.org/specification#response_object">JSON-RPC 2.0 Specification: Response Object</seealso>
    [JsonObject(MemberSerialization.OptIn)]
    public class JsonRpcResponse<TResult> : JsonRpcMessage, IJsonRpcResponse<TResult>
    {

        /// <summary>
        ///     A JSON-RPC response object that indicates success but has a default result value (<c>null</c> for a
        ///     class result type) and the <see cref="JsonRpcId.Null" /> JSON-RPC ID.
        /// </summary>
        public static JsonRpcResponse<TResult> Default { get; } =
            new JsonRpcResponse<TResult>(JsonRpcId.Null, null, default(TResult));


        #region JSON Property Names 

        /// <summary>
        ///     JSON-RPC payload property name of the <see cref="ProtocolVersion" /> property.
        /// </summary>
        public const string ProtocolVersionJsonPropertyName = "jsonrpc";

        /// <summary>
        ///     JSON-RPC payload property name of the <see cref="Error" /> property.
        /// </summary>
        public const string ErrorJsonPropertyName = "error";

        /// <summary>
        ///     JSON-RPC payload property name of the <see cref="Result" /> property.
        /// </summary>
        public const string ResultJsonPropertyName = "result";

        #endregion


        #region Properties 

        /// <inheritdoc />
        /// <summary>
        ///     The JSON-RPC version of the response.
        /// </summary>
        [NotNull]
        [JsonProperty(ProtocolVersionJsonPropertyName, Order = -3 /* Default is -1, ID uses -2. */)]
        public string ProtocolVersion { get; }

        /// <inheritdoc />
        /// <summary>
        ///     Set to the <see cref="T:XploRe.Net.JsonRpc.JsonRpcError" /> instance if an error occured during the JSON-RPC request. If the 
        ///     request succeeded, this property is <c>null</c>.
        /// </summary>
        [JsonProperty(ErrorJsonPropertyName, NullValueHandling = NullValueHandling.Ignore)]
        public JsonRpcError Error { get; }

        /// <inheritdoc />
        /// <summary>
        ///     The arbitrary JSON-RPC result object. May be <c>null</c>. If an error occurred, this property is always
        ///     <c>null</c>. To test for an error indication, use the <see cref="P:XploRe.Net.JsonRpc.JsonRpcResponse`1.IsError" /> property.
        /// </summary>
        [JsonProperty(ResultJsonPropertyName)]
        public TResult Result { get; }

        #endregion


        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Net.JsonRpc.JsonRpcResponse{TResult}" /> instance.
        /// </summary>
        /// <param name="id">The JSON-RPC ID of the response.</param>
        /// <param name="error">The JSON-RPC response error object if an error occurred during the request.</param>
        /// <param name="result">The JSON-RPC response result object if the request succeeded.</param>
        /// <exception cref="T:System.ArgumentException">
        ///     Either <paramref name="error" /> or <paramref name="result" /> must be defined, not both.
        /// </exception>
        public JsonRpcResponse(
            [NotNull] JsonRpcId id,
            [CanBeNull] JsonRpcError error,
            [CanBeNull] TResult result)
            : base(id)
        {
            ProtocolVersion = JsonRpcInternals.ProtocolVersion;

            if (id == null) {
                throw new ArgumentNullException(nameof(id));
            }

            if (id == JsonRpcId.None) {
                throw new ArgumentException("A JSON-RPC response must have an ID.", nameof(id));
            }

            if (error != null && result != null) {
                throw new ArgumentException(
                    $"Either {nameof(error)} or {nameof(result)} must be provided, not both."
                );
            }

            Error = error;
            Result = result;
        }


        #region JSON Serialization

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Net.JsonRpc.JsonRpcResponse{TResult}" /> instance.
        /// </summary>
        /// <param name="id">The JSON-RPC ID of the response.</param>
        /// <param name="protocolVersion">The JSON-RPC version of the response.</param>
        /// <param name="error">The JSON-RPC response error object if an error occurred during the request.</param>
        /// <param name="result">The JSON-RPC response result object if the request succeeded.</param>
        /// <exception cref="T:System.ArgumentException">
        ///     Either <paramref name="error" /> or <paramref name="result" /> must be defined, not both.
        /// </exception>
        [JsonConstructor]
        public JsonRpcResponse(
            [NotNull] JsonRpcId id,
            [NotNull] string protocolVersion,
            [CanBeNull] JsonRpcError error,
            [CanBeNull] TResult result)
            : base(id)
        {
            ProtocolVersion = protocolVersion;

            // Verify constraints but yield a protocol exception instead as this constructor is used by the JSON 
            // deserializer.
            if (id == null || id == JsonRpcId.None) {
                throw new JsonRpcProtocolException("JSON-RPC response is invalid as it contains no ID.");
            }

            if (error != null && result != null) {
                throw new JsonRpcProtocolException(
                    "Invalid JSON-RPC response." +
                    $" Either the '{ErrorJsonPropertyName}' or '{ResultJsonPropertyName}' property must be defined, not both."
                );
            }

            Error = error;
            Result = result;
        }

        /// <summary>
        ///     Controls serialisation of the <see cref="Result" /> property. The error property is required in the
        ///     payload if the response indicates success; the result may be <c>null</c>.
        /// </summary>
        [Pure]
        public bool ShouldSerializeResult() => Error == null;

        #endregion

    }

    /// <inheritdoc />
    /// <summary>
    ///     A JSON-RPC server response which may either indicate success, with an arbitrary response object, or an error
    ///     with a corresponding <see cref="T:XploRe.Net.JsonRpc.JsonRpcError" /> instance describing the error.
    ///     <para>
    ///     This default implementation allows to retrieve a response object of a specified type after the response has
    ///     been deserialised, by using the <see cref="M:XploRe.Net.JsonRpc.JsonRpcResponse.ResultAs``1" /> method.
    ///     </para> 
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class JsonRpcResponse : JsonRpcResponse<object>
    {

        /// <summary>
        ///     A JSON-RPC response object that indicates success with <c>true</c> as the result value and the 
        ///     <see cref="JsonRpcId.Null" /> JSON-RPC ID.
        /// </summary>
        public static JsonRpcResponse<bool> True { get; } = new JsonRpcResponse<bool>(JsonRpcId.Null, null, true);

        /// <summary>
        ///     A JSON-RPC response object that indicates success with <c>false</c> as the result value and the 
        ///     <see cref="JsonRpcId.Null" /> JSON-RPC ID.
        /// </summary>
        public static JsonRpcResponse<bool> False { get; } = new JsonRpcResponse<bool>(JsonRpcId.Null, null, false);

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Net.JsonRpc.JsonRpcResponse" /> instance.
        /// </summary>
        /// <param name="id">The JSON-RPC ID of the response.</param>
        /// <param name="error">The JSON-RPC response error object if an error occurred during the request.</param>
        /// <param name="result">The JSON-RPC response result object if the request succeeded.</param>
        /// <exception cref="T:System.ArgumentException">
        ///     Either <paramref name="error" /> or <paramref name="result" /> must be defined, not both.
        /// </exception>
        public JsonRpcResponse(
            [NotNull] JsonRpcId id,
            [CanBeNull] JsonRpcError error,
            [CanBeNull] object result)
            : base(id, error, result)
        {
        }


        #region JSON Serialization

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Net.JsonRpc.JsonRpcResponse" /> instance.
        /// </summary>
        /// <param name="id">The JSON-RPC ID of the response.</param>
        /// <param name="protocolVersion">The JSON-RPC version of the response.</param>
        /// <param name="error">The JSON-RPC response error object if an error occurred during the request.</param>
        /// <param name="result">The JSON-RPC response result object if the request succeeded.</param>
        /// <exception cref="T:System.ArgumentException">
        ///     Either <paramref name="error" /> or <paramref name="result" /> must be defined, not both.
        /// </exception>
        [JsonConstructor]
        public JsonRpcResponse(
            [NotNull] JsonRpcId id,
            [NotNull] string protocolVersion,
            [CanBeNull] JsonRpcError error,
            [CanBeNull] object result)
            : base(id, protocolVersion, error, result)
        {
        }

        #endregion


        /// <summary>
        ///     Creates an instance of the given type <typeparamref name="T" /> from the result of the response. 
        /// </summary>
        /// <typeparam name="T">The desired object type to convert the response result to.</typeparam>
        /// <returns>An instance of the desired type <typeparamref name="T" /> created from the response result.</returns>
        [Pure]
        [CanBeNull]
        public T ResultAs<T>()
        {
            switch (Result) {
            case T result:
                // If the type of the result already matches, return it. This should cover all primitive types and their
                // implicit casts.
                return result;

            case JToken token:
                // By default, Json.NET yields a JToken for arrays and structed types, which we can convert using the 
                // default Json.NET converters.
                return token.ToObject<T>();
            }

            return (T) Convert.ChangeType(Result, typeof(T));
        }


        #region Factories 

        /// <summary>
        ///     Creates a new JSON-RPC error response instance with a <see cref="JsonRpcId.Null" /> ID.
        /// </summary>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse WithError(int code, [NotNull] string message)
            => WithError(new JsonRpcError(code, message, null));

        /// <summary>
        ///     Creates a new JSON-RPC response instance that indicates an error.
        /// </summary>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <param name="id">The JSON-RPC ID of the request that failed.</param>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse WithError(int code, [NotNull] string message, [NotNull] JsonRpcId id)
            => WithError(new JsonRpcError(code, message, null), id);

        /// <summary>
        ///     Creates a new JSON-RPC error response instance with a <see cref="JsonRpcId.Null" /> ID.
        /// </summary>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <param name="data">
        ///     An optional primitive or structured value with additional information about the error.
        /// </param>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse WithError(int code, [NotNull] string message, [CanBeNull] object data)
            => WithError(new JsonRpcError(code, message, data));

        /// <summary>
        ///     Creates a new JSON-RPC response instance that indicates an error.
        /// </summary>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <param name="data">
        ///     An optional primitive or structured value with additional information about the error.
        /// </param>
        /// <param name="id">The JSON-RPC ID of the request that failed.</param>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse WithError(
            int code, [NotNull] string message, [CanBeNull] object data,
            [NotNull] JsonRpcId id)
            => WithError(new JsonRpcError(code, message, data), id);

        /// <summary>
        ///     Creates a new JSON-RPC error response instance with a <see cref="JsonRpcId.Null" /> ID.
        /// </summary>
        /// <param name="jsonRpcError">The <see cref="JsonRpcError" /> instance that describes the error.</param>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse WithError([NotNull] JsonRpcError jsonRpcError)
            => WithError(jsonRpcError, JsonRpcId.Null);

        /// <summary>
        ///     Creates a new JSON-RPC response instance that indicates an error.
        /// </summary>
        /// <param name="jsonRpcError">The <see cref="JsonRpcError" /> instance that describes the error.</param>
        /// <param name="id">The JSON-RPC ID of the request that failed.</param>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse WithError([NotNull] JsonRpcError jsonRpcError, [NotNull] JsonRpcId id)
            => new JsonRpcResponse(id, jsonRpcError, null);

        /// <summary>
        ///     Creates a new JSON-RPC error response instance with a <see cref="JsonRpcId.Null" /> ID.
        /// </summary>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <typeparam name="T">The desired type of the reponse result.</typeparam>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> WithError<T>(int code, [NotNull] string message)
            => WithError<T>(new JsonRpcError(code, message, null));

        /// <summary>
        ///     Creates a new JSON-RPC response instance that indicates an error.
        /// </summary>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <param name="id">The JSON-RPC ID of the request that failed.</param>
        /// <typeparam name="T">The desired type of the reponse result.</typeparam>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> WithError<T>(int code, [NotNull] string message, [NotNull] JsonRpcId id)
            => WithError<T>(new JsonRpcError(code, message, null), id);

        /// <summary>
        ///     Creates a new JSON-RPC error response instance with a <see cref="JsonRpcId.Null" /> ID.
        /// </summary>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <param name="data">
        ///     An optional primitive or structured value with additional information about the error.
        /// </param>
        /// <typeparam name="T">The desired type of the reponse result.</typeparam>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> WithError<T>(int code, [NotNull] string message, [CanBeNull] object data)
            => WithError<T>(new JsonRpcError(code, message, data));

        /// <summary>
        ///     Creates a new JSON-RPC response instance that indicates an error.
        /// </summary>
        /// <param name="code">The JSON-RPC error code.</param>
        /// <param name="message">A short description <see cref="string" /> of the error.</param>
        /// <param name="data">
        ///     An optional primitive or structured value with additional information about the error.
        /// </param>
        /// <param name="id">The JSON-RPC ID of the request that failed.</param>
        /// <typeparam name="T">The desired type of the reponse result.</typeparam>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> WithError<T>(
            int code, [NotNull] string message, [CanBeNull] object data,
            [NotNull] JsonRpcId id)
            => WithError<T>(new JsonRpcError(code, message, data), id);

        /// <summary>
        ///     Creates a new JSON-RPC error response instance with a <see cref="JsonRpcId.Null" /> ID.
        /// </summary>
        /// <param name="jsonRpcError">The <see cref="JsonRpcError" /> instance that describes the error.</param>
        /// <typeparam name="T">The desired type of the reponse result.</typeparam>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> WithError<T>([NotNull] JsonRpcError jsonRpcError)
            => WithError<T>(jsonRpcError, JsonRpcId.Null);

        /// <summary>
        ///     Creates a new JSON-RPC response instance that indicates an error.
        /// </summary>
        /// <param name="jsonRpcError">The <see cref="JsonRpcError" /> instance that describes the error.</param>
        /// <param name="id">The JSON-RPC ID of the request that failed.</param>
        /// <typeparam name="T">The desired type of the reponse result.</typeparam>
        /// <returns>A new JSON-RPC response that contains the provided error details.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> WithError<T>([NotNull] JsonRpcError jsonRpcError, [NotNull] JsonRpcId id)
            => new JsonRpcResponse<T>(id, jsonRpcError, default(T));

        /// <summary>
        ///     Creates a new JSON-RPC response with the given result object and the <see cref="JsonRpcId.Null" /> ID.
        /// </summary>
        /// <param name="result">The result object of the response.</param>
        /// <typeparam name="T">The type of the <paramref name="result" /> parameter.</typeparam>
        /// <returns>A new JSON-RPC response that contains the provided result object.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> WithResult<T>([CanBeNull] T result)
            => WithResult(result, JsonRpcId.Null);

        /// <summary>
        ///     Creates a new JSON-RPC response with the given result object.
        /// </summary>
        /// <param name="result">The result object of the response.</param>
        /// <param name="id">The JSON-RPC ID of the request that succeeded.</param>
        /// <typeparam name="T">The type of the <paramref name="result" /> parameter.</typeparam>
        /// <returns>A new JSON-RPC response that contains the provided result object.</returns>
        [Pure]
        [NotNull]
        public static JsonRpcResponse<T> WithResult<T>([CanBeNull] T result, [NotNull] JsonRpcId id)
            => new JsonRpcResponse<T>(id, null, result);

        #endregion

    }

}
