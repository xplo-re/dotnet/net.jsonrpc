﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.ComponentModel;
using XploRe.Runtime;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc />
    /// <summary>
    ///     Standard JSON-RPC error codes and ranges as defined by the RFC.
    /// </summary>
    /// <seealso href="http://www.jsonrpc.org/specification#error_object">JSON-RPC 2.0 Specification: Error Object</seealso>
    public class JsonRpcErrorCodes : GenericEnum<int, JsonRpcErrorCodes>
    {

        /// <summary>
        /// 	Error code returned by the server if invalid JSON was received or an error occurred while parsing 
        ///     the JSON payload.
        /// </summary>
        [Description("Parser error.")]
        public static readonly JsonRpcErrorCodes ParseError = new JsonRpcErrorCodes(-32700);

        /// <summary>
        /// 	Error code returned by the server if the JSON sent is not a valid request object.
        /// </summary>
        [Description("Invalid request.")]
        public static readonly JsonRpcErrorCodes InvalidRequest = new JsonRpcErrorCodes(-32600);

        /// <summary>
        ///    Error code returned by the server if the method does not exist or is not available.
        /// </summary>
        [Description("Method not found.")]
        public static readonly JsonRpcErrorCodes MethodNotFound = new JsonRpcErrorCodes(-32601);

        /// <summary>
        /// 	Error code returned by the server if the method parameter(s) are invalid.
        /// </summary>
        [Description("Invalid parameters.")]
        public static readonly JsonRpcErrorCodes InvalidParameters = new JsonRpcErrorCodes(-32602);

        /// <summary>
        /// 	Internal JSON-RPC error code.
        /// </summary>
        [Description("Internal error.")]
        public static readonly JsonRpcErrorCodes InternalError = new JsonRpcErrorCodes(-32603);

        /// <summary>
        /// 	Upper limit of the range of error codes that are reserved for implementation-defined server-errors.
        /// </summary>
        public const int ServerErrorRangeMax = -32000;

        /// <summary>
        /// 	Lower limit of the range of error codes that are reserved for implementation-defined server-errors.
        /// </summary>
        public const int ServerErrorRangeMin = -32099;

        /// <summary>
        ///     Determines whether a given error code is from the implementation-defined server-errors range.
        /// </summary>
        /// <param name="code">The error code to test.</param>
        /// <returns><c>true</c>, if <paramref name="code" /> is a server error, otherwise <c>false</c>.</returns>
        public static bool IsServerError(int code) => code >= ServerErrorRangeMin && code <= ServerErrorRangeMax;

        /// <inheritdoc />
        protected JsonRpcErrorCodes(int value) : base(value)
        {
        }

    }

}
