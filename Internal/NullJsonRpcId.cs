﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

namespace XploRe.Net.JsonRpc.Internal
{

    /// <inheritdoc />
    /// <summary>
    ///     JSON-RPC message ID that represents a <c>null</c> value.
    /// </summary>
    internal class NullJsonRpcId : JsonRpcId
    {

        /// <inheritdoc />
        public override JsonRpcIdType Type => JsonRpcIdType.Null;

        /// <inheritdoc />
        public override object Value => null;

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (obj == null) {
                return true;
            }

            return obj is NullJsonRpcId;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return 0;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return "(null)";
        }

    }

}
