﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Newtonsoft.Json;
using XploRe.Json.Internal;
using XploRe.Reflection;


namespace XploRe.Net.JsonRpc.Internal
{

    internal class JsonRpcBatchConverter<T> : JsonConverter where T : IJsonRpcMessage
    {

        /// <inheritdoc />
        [Pure]
        public override bool CanConvert(Type objectType)
        {
            if (objectType == null) {
                return false;
            }

            return objectType.IsSameOrSubclassOf(typeof(JsonRpcBatch<T>));
        }


        #region Reading Values 

        /// <inheritdoc />
        public override bool CanRead => true;

        /// <inheritdoc />
        public override object ReadJson(
            [NotNull] JsonReader reader,
            [NotNull] Type objectType,
            object existingValue,
            [NotNull] JsonSerializer serializer)
        {
            if (reader == null) {
                throw new ArgumentNullException(nameof(reader));
            }

            if (objectType == null) {
                throw new ArgumentNullException(nameof(objectType));
            }

            if (serializer == null) {
                throw new ArgumentNullException(nameof(serializer));
            }

            if (!reader.MoveToContent()) {
                // Empty streams are handled by Json.NET before invoking a converter (and will always return null),
                // hence an empty stream, which is valid for a batch, would never occur here.
                throw JsonHelper.CreateSerializationException(
                    reader,
                    "Premature end of stream when reading JSON-RPC message batch.",
                    (msg, e) => new JsonRpcProtocolException(msg, e)
                );
            }

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (reader.TokenType) {
            case JsonToken.StartArray:
                // A batch of messages.
                return ReadBatch(reader, serializer);

            case JsonToken.StartObject:
                // A single message, wrap in batch.
                return ReadMessage(reader, serializer);
            }

            throw JsonHelper.CreateSerializationException(
                reader,
                "Expected array or object token when reading JSON-RPC message batch.",
                (msg, e) => new JsonRpcProtocolException(msg, e)
            );
        }

        [NotNull]
        private static JsonRpcBatch<T> ReadBatch([NotNull] JsonReader reader, [NotNull] JsonSerializer serializer)
        {
            var messages = serializer.Deserialize<T[]>(reader);

            return new JsonRpcBatch<T>(messages);
        }

        [NotNull]
        private static JsonRpcBatch<T> ReadMessage([NotNull] JsonReader reader, [NotNull] JsonSerializer serializer)
        {
            var message = serializer.Deserialize<T>(reader);

            return new JsonRpcBatch<T> { message };
        }

        #endregion


        #region Writing Values 

        /// <inheritdoc />
        public override bool CanWrite => true;

        /// <inheritdoc />
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (writer == null) {
                throw new ArgumentNullException(nameof(writer));
            }

            if (serializer == null) {
                throw new ArgumentNullException(nameof(serializer));
            }

            switch (value) {
            case JsonRpcBatch<T> batch:
                switch (batch.Count) {
                case 0:
                    // Empty batch arrays are invalid according to the JSON-RFC; yield an empty string.
                    return;

                case 1:
                    // Instead of encoding a batch, encode a single message.
                    serializer.Serialize(writer, batch[0]);
                    break;

                default:
                    writer.WriteStartArray();

                    foreach (var message in batch) {
                        serializer.Serialize(writer, message);
                    }

                    writer.WriteEndArray();
                    break;
                }
                break;

            default:
                throw new ArgumentException(
                    $"Argument is not of expected type '{typeof(JsonRpcBatch<T>)}'.", nameof(value)
                );
            }
        }

        #endregion

    }

}
