﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

namespace XploRe.Net.JsonRpc.Internal
{

    /// <inheritdoc />
    /// <summary>
    ///     JSON-RPC message ID that is used for notification messages that, by definition, omit the message ID in 
    ///     the generated payload.
    /// </summary>
    internal class NoneJsonRpcId : JsonRpcId
    {

        /// <inheritdoc />
        public override JsonRpcIdType Type => JsonRpcIdType.None;

        /// <inheritdoc />
        public override object Value => null;

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            // Only one instance of this specific type of JSON-RPC ID exists, as it cannot be represented by
            // native .NET Types.
            return ReferenceEquals(this, obj);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return -1;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return string.Empty;
        }

    }

}
