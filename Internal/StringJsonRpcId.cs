﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Diagnostics;
using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc.Internal
{

    /// <inheritdoc />
    /// <summary>
    ///     JSON-RPC message ID that represents a non-<c>null</c> string value.
    /// </summary>
    internal class StringJsonRpcId : JsonRpcId
    {

        [NotNull]
        private readonly string _value;

        /// <inheritdoc />
        public override JsonRpcIdType Type => JsonRpcIdType.String;

        /// <inheritdoc />
        [NotNull]
        public override object Value => _value;

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new string JSON-RPC message ID with the given string value.
        /// </summary>
        /// <param name="value">Value to initialise message ID with.</param>
        internal StringJsonRpcId([NotNull] string value)
        {
            Debug.Assert(value != null, $"{nameof(value)} != null");

            _value = value;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            switch (obj) {
            case StringJsonRpcId other:
                return Value.Equals(other.Value);

            case string other:
                return Value.Equals(other);
            }

            return false;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return _value;
        }

    }

}
