﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using JetBrains.Annotations;
using Newtonsoft.Json;


namespace XploRe.Net.JsonRpc.Internal
{

    /// <inheritdoc cref="JsonRpcRequest" />
    /// <summary>
    ///     JSON-RPC request message with positional parameters.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    internal class ParametersCollectionJsonRpcRequest : JsonRpcRequest, IHasParametersCollection
    {

        /// <inheritdoc />
        /// <summary>
        ///     Read-only collection of ordered request parameters.
        /// </summary>
        [JsonProperty(ParametersJsonPropertyName)]
        public IReadOnlyCollection<object> Parameters { get; }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new JSON-RPC request instance with positional parameters.
        /// </summary>
        /// <param name="id">ID of RPC request.</param>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="parameters">Enumerable of RPC request parameters.</param>
        public ParametersCollectionJsonRpcRequest(
            [NotNull] JsonRpcId id,
            [NotNull] string method,
            [NotNull] [ItemCanBeNull] IEnumerable<object> parameters)
            : base(id, method)
        {
            if (parameters == null) {
                throw new ArgumentNullException(nameof(parameters));
            }

            Parameters = new ReadOnlyCollection<object>(parameters.ToList());
        }

        /// <summary>
        ///     Controls serialisation of the <see cref="Parameters" /> property. The parameters property is omitted 
        ///     from the payload if the collection is empty.
        /// </summary>
        public bool ShouldSerializeParameters() => Parameters.Count > 0;

    }

}
