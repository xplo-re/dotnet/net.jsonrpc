﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using XploRe.Json.Internal;
using XploRe.Runtime;


namespace XploRe.Net.JsonRpc.Internal
{

    /// <inheritdoc />
    /// <summary>
    ///     Writes <see cref="T:XploRe.Net.JsonRpc.JsonRpcId" /> instances as a primitive JSON value and reads-back
    ///     JSON-RPC message IDs by automatically boxing them as <see cref="T:XploRe.Net.JsonRpc.JsonRpcId" /> 
    ///     instances from their primitive JSON values.
    /// </summary>
    internal class JsonRpcIdJsonConverter : JsonConverter
    {

        /// <inheritdoc />
        public override bool CanConvert(Type objectType)
        {
            return typeof(JsonRpcId) == objectType;
        }


        #region Reading Values

        /// <inheritdoc />
        public override bool CanRead => true;

        /// <inheritdoc />
        [NotNull]
        public override object ReadJson(
            [NotNull] JsonReader reader,
            [NotNull] Type objectType,
            object existingValue,
            [NotNull] JsonSerializer serializer)
        {
            if (reader == null) {
                throw new ArgumentNullException(nameof(reader));
            }

            if (objectType == null) {
                throw new ArgumentNullException(nameof(objectType));
            }

            if (serializer == null) {
                throw new ArgumentNullException(nameof(serializer));
            }

            if (!reader.MoveToContent()) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    $"Unexpected end when reading {typeof(JsonRpcId)}."
                );
            }

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (reader.TokenType) {
            case JsonToken.String:
                if (!(reader.Value is string stringValue)) {
                    throw new RuntimeInconsistencyException(
                        "String JSON token value is null."
                    );
                }

                return JsonRpcId.FromString(stringValue);

            case JsonToken.Integer:
                if (reader.Value == null) {
                    throw new RuntimeInconsistencyException(
                        "Integer JSON token value is null."
                    );
                }

                return JsonRpcId.FromNumber((long) reader.Value);

            case JsonToken.Float:
                if (reader.Value == null) {
                    throw new RuntimeInconsistencyException(
                        "Float JSON token value is null."
                    );
                }

                return JsonRpcId.FromNumber((double) reader.Value);

            case JsonToken.Null:
                return JsonRpcId.Null;

            default:
                throw new JsonException(
                    $"JSON token type {reader.TokenType} not allowed for a JSON-RPC message ID."
                );
            }
        }

        #endregion


        #region Writing Values

        /// <inheritdoc />
        public override bool CanWrite => true;

        /// <inheritdoc />
        public override void WriteJson(
            [NotNull] JsonWriter writer,
            object value,
            [NotNull] JsonSerializer serializer)
        {
            if (writer == null) {
                throw new ArgumentNullException(nameof(writer));
            }

            if (serializer == null) {
                throw new ArgumentNullException(nameof(serializer));
            }

            var id = value as JsonRpcId;

            if (id == null) {
                throw new ArgumentException($"Object is not a {typeof(JsonRpcId)} instance.", nameof(value));
            }

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (id.Type) {
            case JsonRpcIdType.None:
                return;

            case JsonRpcIdType.Null:
                writer.WriteNull();
                return;
            }

            var token = JToken.FromObject(id.Value);

            if (token == null) {
                throw new RuntimeInconsistencyException(
                    "{0}.{1}() returned null for JSON-RPC ID value '{2}'.".FormatWith(
                        typeof(JToken), nameof(JToken.FromObject), id.Value
                    )
                );
            }

            token.WriteTo(writer);
        }

        #endregion

    }

}
