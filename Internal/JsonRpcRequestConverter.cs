﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Newtonsoft.Json;
using XploRe.Json;
using XploRe.Json.Internal;
using XploRe.Reflection;


namespace XploRe.Net.JsonRpc.Internal
{

    internal class JsonRpcRequestConverter : JsonConverter
    {

        [Pure]
        public override bool CanConvert(Type objectType)
        {
            if (objectType == null) {
                return false;
            }

            return objectType.IsSameOrSubclassOf(typeof(IJsonRpcRequest));
        }


        #region Reading Values 

        public override bool CanRead => true;

        public override object ReadJson(
            [NotNull] JsonReader reader,
            [NotNull] Type objectType,
            object existingValue,
            [NotNull] JsonSerializer serializer)
        {
            if (reader == null) {
                throw new ArgumentNullException(nameof(reader));
            }

            if (objectType == null) {
                throw new ArgumentNullException(nameof(objectType));
            }

            if (serializer == null) {
                throw new ArgumentNullException(nameof(serializer));
            }

            return ReadRequest(reader, serializer);
        }

        private static object ReadRequest([NotNull] JsonReader reader, [NotNull] JsonSerializer serializer)
        {
            if (!reader.MoveToContent()) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    "Premature end of stream when reading JSON-RPC request.",
                    (msg, e) => new JsonRpcProtocolException(msg, e)
                );
            }

            if (reader.TokenType != JsonToken.StartObject) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    "Expected start of object token when reading JSON-RPC request.",
                    (msg, e) => new JsonRpcProtocolException(msg, e)
                );
            }

            JsonRpcId id = null;
            string method = null;
            object parameters = null;

            while (reader.Read()) {
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (reader.TokenType) {
                case JsonToken.PropertyName:
                    var propertyName = reader.Value?.ToString();

                    switch (propertyName) {
                    case JsonRpcMessage.IdJsonPropertyName:
                        reader.Read();
                        id = serializer.Deserialize<JsonRpcId>(reader);
                        continue;

                    case JsonRpcRequest.ProtocolVersionJsonPropertyName:
                        ReadVersion(reader, id);
                        continue;

                    case JsonRpcRequest.MethodJsonPropertyName:
                        method = ReadMethod(reader, id);
                        continue;

                    case JsonRpcRequest.ParametersJsonPropertyName:
                        parameters = ReadParameters(reader, id, serializer);
                        continue;

                    default:
                        throw JsonHelper.CreateSerializationException(
                            reader,
                            $"Encountered unsupported property '{propertyName}' when reading JSON-RPC request.",
                            (msg, e) => new JsonRpcProtocolException(msg, id, e)
                        );
                    }

                case JsonToken.EndObject:
                    if (id == null) {
                        // No optional ID encountered, treat as notification.
                        id = JsonRpcId.None;
                    }

                    if (string.IsNullOrEmpty(method)) {
                        throw JsonHelper.CreateSerializationException(
                            reader,
                            "Method name not found when reading JSON-RPC request.",
                            (msg, e) => new JsonRpcProtocolException(msg, id, e)
                        );
                    }

                    switch (parameters) {
                    case IDictionary<string, object> dictionary:
                        return JsonRpcRequest.WithMethod(method, dictionary, id);

                    case ICollection<object> list:
                        return JsonRpcRequest.WithMethod(method, list, id);

                    case null:
                        return new JsonRpcRequest(id, method);

                    default:
                        throw JsonHelper.CreateSerializationException(
                            reader,
                            $"Encountered parameters property of invalid type '{parameters.GetType()}' when reading JSON-RPC request.",
                            (msg, e) => new JsonRpcProtocolException(msg, id, e)
                        );
                    }
                }
            }

            throw JsonHelper.CreateSerializationException(
                reader,
                $"Unexpected token '{reader.TokenType}' encountered when reading JSON-RPC request.",
                (msg, e) => new JsonRpcProtocolException(msg, id, e)
            );
        }

        [NotNull]
        private static string ReadMethod([NotNull] JsonReader reader, [CanBeNull] JsonRpcId id)
        {
            if (!reader.ReadAndMoveToContent()) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    "Premature end of stream when reading JSON-RPC method value.",
                    (msg, e) => new JsonRpcProtocolException(msg, id, e)
                );
            }

            if (!(reader.Value is string)) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    $"Encountered method name property of invalid type '{reader.ValueType}' when reading JSON-RPC request.",
                    (msg, e) => new JsonRpcProtocolException(msg, id, e)
                );
            }

            var method = (string) reader.Value;

            if (method == null) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    "Method name was null when reading JSON-RPC request.",
                    (msg, e) => new JsonRpcProtocolException(msg, id, e)
                );
            }

            return method;
        }

        [NotNull]
        private static object ReadParameters(
            [NotNull] JsonReader reader,
            [CanBeNull] JsonRpcId id,
            [NotNull] JsonSerializer serializer)
        {
            if (!reader.ReadAndMoveToContent()) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    "Premature end of stream when reading JSON-RPC request parameters value.",
                    (msg, e) => new JsonRpcProtocolException(msg, id, e)
                );
            }

            var converter = new CollectionConverter();
            var result = converter.ReadJson(reader, typeof(object), null, serializer);

            if (result == null) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    "Failed to deserialize parameters for JSON-RPC request.",
                    (msg, e) => new JsonRpcProtocolException(msg, id, e)
                );
            }

            return result;
        }

        private static void ReadVersion([NotNull] JsonReader reader, [CanBeNull] JsonRpcId id)
        {
            if (!reader.ReadAndMoveToContent()) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    "Premature end of stream when reading JSON-RPC version value.",
                    (msg, e) => new JsonRpcProtocolException(msg, id, e)
                );
            }

            if (!(reader.Value is string)) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    $"Encountered JSON-RPC version property of invalid type '{reader.ValueType}' when reading JSON-RPC request.",
                    (msg, e) => new JsonRpcProtocolException(msg, id, e)
                );
            }

            var version = (string) reader.Value;

            if (version != JsonRpcInternals.ProtocolVersion) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    $"Encountered unsupported JSON-RPC version '{version}' when reading JSON-RPC request.",
                    (msg, e) => new JsonRpcProtocolException(msg, id, e)
                );
            }
        }

        #endregion


        #region Writing Values 

        public override bool CanWrite => false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new InvalidOperationException("Converter does not support writing.");
        }

        #endregion

    }

}
