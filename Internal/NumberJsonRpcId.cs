﻿using System;
using JetBrains.Annotations;


/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */
namespace XploRe.Net.JsonRpc.Internal
{

    /// <inheritdoc />
    /// <summary>
    ///     JSON-RPC message ID that represents a numeric (integer or fractional) value.
    /// </summary>
    internal class NumberJsonRpcId : JsonRpcId
    {

        [NotNull]
        private readonly ValueType _value;

        /// <inheritdoc />
        public override JsonRpcIdType Type => JsonRpcIdType.Number;

        /// <inheritdoc />
        [NotNull]
        public override object Value => _value;

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new numeric JSON-RPC message ID with the given numeric value.
        /// </summary>
        /// <param name="value">Value to initialise message ID with.</param>
        internal NumberJsonRpcId([NotNull] ValueType value)
        {
            _value = value;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            // Explicitly cast smaller types to the corresponding larger type as it is implicitly casted for use
            // with RPC IDs. Otherwise the Equal() method will return false.
            switch (obj) {
            case NumberJsonRpcId other:
                return Value.Equals(other.Value);

            case sbyte other:
                return Value.Equals((long) other);

            case byte other:
                return Value.Equals((long) other);

            case short other:
                return Value.Equals((long) other);

            case ushort other:
                return Value.Equals((long) other);

            case int other:
                return Value.Equals((long) other);

            case uint other:
                return Value.Equals((long) other);

            case long other:
                return Value.Equals(other);

            case ulong other:
                return Value.Equals((double) other);

            case float other:
                return Value.Equals((double) other);

            case double other:
                return Value.Equals(other);
            }

            return false;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return _value.ToString() ?? string.Empty;
        }

    }

}
