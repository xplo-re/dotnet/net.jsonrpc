﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

namespace XploRe.Net.JsonRpc.Internal
{

    internal static class JsonRpcInternals
    {

        /// <summary>
        ///     The version of the JSON-RPC protocol that this library implements. 
        /// </summary>
        public const string ProtocolVersion = "2.0";

    }

}
