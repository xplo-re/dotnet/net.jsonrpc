﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.Annotations;
using Newtonsoft.Json;


namespace XploRe.Net.JsonRpc.Internal
{

    /// <inheritdoc cref="JsonRpcRequest" />
    /// <summary>
    ///     JSON-RPC request message with named parameters.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    internal class ParametersDictionaryJsonRpcRequest : JsonRpcRequest, IHasParametersDictionary
    {

        /// <inheritdoc />
        /// <summary>
        ///     Read-only dictionary of request parameter names and their corresponding values.
        /// </summary>
        [JsonProperty(ParametersJsonPropertyName)]
        public IReadOnlyDictionary<string, object> Parameters { get; }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new JSON-RPC request instance with named parameters.
        /// </summary>
        /// <param name="id">ID of RPC request.</param>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="parameters">Dictionary of RPC request parameters.</param>
        public ParametersDictionaryJsonRpcRequest(
            [NotNull] JsonRpcId id,
            [NotNull] string method,
            [NotNull] IDictionary<string, object> parameters)
            : base(id, method)
        {
            if (parameters == null) {
                throw new ArgumentNullException(nameof(parameters));
            }

            Parameters = new ReadOnlyDictionary<string, object>(parameters);
        }

        /// <summary>
        ///     Controls serialisation of the <see cref="Parameters" /> property. The parameters property is omitted
        ///     from the payload if the dictionary is empty.
        /// </summary>
        public bool ShouldSerializeParameters() => Parameters.Count > 0;

    }

}
