﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc cref="IJsonRpcMessage" />
    /// <summary>
    ///     Base interface for JSON-RPC requests.
    /// </summary>
    public interface IJsonRpcRequest : IJsonRpcMessage, IHasProtocolVersion
    {

        /// <summary>
        ///     The name of the method invoked by the request.
        /// </summary>
        [NotNull]
        string Method { get; }

    }

}
