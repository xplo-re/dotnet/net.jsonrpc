﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Newtonsoft.Json;
using XploRe.Net.JsonRpc.Internal;
using XploRe.Runtime;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc cref="JsonRpcMessage" />
    /// <summary>
    ///     JSON-RPC request message.
    /// </summary>
    /// <seealso href="http://www.jsonrpc.org/specification#request_object">JSON-RPC 2.0 Specification: Request Object</seealso>
    [JsonObject(MemberSerialization.OptIn)]
    [JsonConverter(typeof(JsonRpcRequestConverter))]
    public class JsonRpcRequest : JsonRpcMessage, IJsonRpcRequest
    {

        #region JSON Property Names 

        /// <summary>
        ///     JSON-RPC property name of the <see cref="Method" /> property.
        /// </summary>
        public const string MethodJsonPropertyName = "method";

        /// <summary>
        ///     JSON-RPC property name of the <see cref="IHasParametersCollection.Parameters" /> or 
        ///     <see cref="IHasParametersDictionary.Parameters"/> properties of specialised request instances.
        /// </summary>
        public const string ParametersJsonPropertyName = "params";

        /// <summary>
        ///     JSON-RPC property name of the <see cref="ProtocolVersion" /> property.
        /// </summary>
        public const string ProtocolVersionJsonPropertyName = "jsonrpc";

        #endregion


        /// <summary>
        ///     Reserved method prefix for internal or extension RPC methods. 
        /// </summary>
        public const string InternalMethodPrefix = "rpc.";


        #region Properties 

        /// <inheritdoc />
        /// <summary>
        ///     The JSON-RPC version of the request.
        /// </summary>
        [NotNull]
        [JsonProperty(ProtocolVersionJsonPropertyName, Order = -3 /* Default is -1, ID uses -2. */)]
        public string ProtocolVersion => JsonRpcInternals.ProtocolVersion;

        /// <inheritdoc />
        /// <summary>
        /// <param>
        ///     The name of the method invoked by the request.
        /// </param>
        /// <param>
        ///     Method names that start with "rpc." are RPC-internal and extension methods and must not be used for any
        ///     other purposes. 
        /// </param>
        /// </summary>
        /// <seealso cref="JsonRpcRequestExtensions.IsInternal" />
        [JsonProperty(MethodJsonPropertyName)]
        public string Method { get; }

        #endregion


        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new parameterless JSON-RPC request instance.
        /// </summary>
        /// <param name="id">ID of RPC request.</param>
        /// <param name="method">Name of RPC method.</param>
        public JsonRpcRequest([NotNull] JsonRpcId id, [NotNull] string method) : base(id)
        {
            if (method == null) {
                throw new ArgumentNullException(nameof(method));
            }

            if (string.IsNullOrEmpty(method)) {
                throw new ArgumentStringEmptyException(nameof(method));
            }

            Method = method;
        }


        #region Factories

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name and no request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> notification instance.</returns>
        [NotNull]
        public static JsonRpcRequest NotificationWithMethod([NotNull] string method)
        {
            return WithMethod(method, JsonRpcId.None);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided positional
        ///     parameters and no request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="positionalParameters">Array of positional request parameters.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> notification instance.</returns>
        [NotNull]
        public static JsonRpcRequest NotificationWithMethod<T>(
            [NotNull] string method,
            [CanBeNull] [ItemCanBeNull] T[] positionalParameters)
        {
            return WithMethod(method, positionalParameters?.Cast<object>());
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided positional
        ///     parameters and no request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="positionalParameters">Enumerable of positional request parameters.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> notification instance.</returns>
        [NotNull]
        public static JsonRpcRequest NotificationWithMethod(
            [NotNull] string method,
            [CanBeNull] [ItemCanBeNull] IEnumerable<object> positionalParameters)
        {
            return WithMethod(method, positionalParameters, JsonRpcId.None);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided positional
        ///     parameters and no request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="positionalParameters">Collection of positional request parameters.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> notification instance.</returns>
        [NotNull]
        public static JsonRpcRequest NotificationWithMethod(
            [NotNull] string method,
            [CanBeNull] [ItemCanBeNull] ICollection<object> positionalParameters)
        {
            return WithMethod(method, positionalParameters, JsonRpcId.None);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided named
        ///     parameters and no request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="namedParameters">Dictionary of named request parameters.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> notification instance.</returns>
        [NotNull]
        public static JsonRpcRequest NotificationWithMethod(
            [NotNull] string method,
            [CanBeNull] IDictionary<string, object> namedParameters)
        {
            return WithMethod(method, namedParameters, JsonRpcId.None);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name and the next automatic
        ///     per-process sequential request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> instance.</returns>
        [NotNull]
        public static JsonRpcRequest WithMethod([NotNull] string method)
        {
            var id = JsonRpcId.NextSequential();

            return WithMethod(method, id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name and a specified request 
        ///     ID.
        /// </summary>
        /// <param name="id">ID of RPC request.</param>
        /// <param name="method">Name of RPC method.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> instance.</returns>
        [NotNull]
        public static JsonRpcRequest WithMethod([NotNull] string method, [NotNull] JsonRpcId id)
        {
            return new JsonRpcRequest(id, method);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided positional
        ///     parameters and the next automatic per-process sequential request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="positionalParameters">Array of positional request parameters.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> instance.</returns>
        [NotNull]
        public static JsonRpcRequest WithMethod<T>(
            [NotNull] string method,
            [CanBeNull] [ItemCanBeNull] T[] positionalParameters)
        {
            return WithMethod(method, positionalParameters?.Cast<object>());
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided positional
        ///     parameters and the next automatic per-process sequential request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="positionalParameters">Enumerable of positional request parameters.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> instance.</returns>
        [NotNull]
        public static JsonRpcRequest WithMethod(
            [NotNull] string method,
            [CanBeNull] [ItemCanBeNull] IEnumerable<object> positionalParameters)
        {
            var id = JsonRpcId.NextSequential();

            return WithMethod(method, positionalParameters, id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided positional
        ///     parameters and the next automatic per-process sequential request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="positionalParameters">Collection of positional request parameters.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> instance.</returns>
        [NotNull]
        public static JsonRpcRequest WithMethod(
            [NotNull] string method,
            [CanBeNull] [ItemCanBeNull] ICollection<object> positionalParameters)
        {
            var id = JsonRpcId.NextSequential();

            return WithMethod(method, positionalParameters, id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided positional
        ///     parameters and the specified request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="positionalParameters">Array of positional request parameters.</param>
        /// <param name="id">ID of RPC request.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> instance.</returns>
        [NotNull]
        public static JsonRpcRequest WithMethod<T>(
            [NotNull] string method,
            [CanBeNull] [ItemCanBeNull] T[] positionalParameters,
            [NotNull] JsonRpcId id)
        {
            return WithMethod(method, positionalParameters?.Cast<object>(), id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided positional
        ///     parameters and the specified request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="positionalParameters">Enumerable of positional request parameters.</param>
        /// <param name="id">ID of RPC request.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> instance.</returns>
        [NotNull]
        public static JsonRpcRequest WithMethod(
            [NotNull] string method,
            [CanBeNull] [ItemCanBeNull] IEnumerable<object> positionalParameters,
            [NotNull] JsonRpcId id)
        {
            return WithMethod(method, positionalParameters?.ToList(), id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided positional
        ///     parameters and the specified request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="positionalParameters">Collection of positional request parameters.</param>
        /// <param name="id">ID of RPC request.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> instance.</returns>
        [NotNull]
        public static JsonRpcRequest WithMethod(
            [NotNull] string method,
            [CanBeNull] [ItemCanBeNull] ICollection<object> positionalParameters,
            [NotNull] JsonRpcId id)
        {
            if (positionalParameters == null || positionalParameters.Count == 0) {
                return new JsonRpcRequest(id, method);
            }

            return new ParametersCollectionJsonRpcRequest(id, method, positionalParameters);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided named
        ///     parameters and the next automatic per-process sequential request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="namedParameters">Dictionary of named request parameters.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> instance.</returns>
        [NotNull]
        public static JsonRpcRequest WithMethod(
            [NotNull] string method,
            [CanBeNull] IDictionary<string, object> namedParameters)
        {
            var id = JsonRpcId.NextSequential();

            return WithMethod(method, namedParameters, id);
        }

        /// <summary>
        ///     Creates a new <see cref="JsonRpcRequest" /> instance with the given method name, the provided named
        ///     parameters and the specified request ID.
        /// </summary>
        /// <param name="method">Name of RPC method.</param>
        /// <param name="namedParameters">Dictionary of named request parameters.</param>
        /// <param name="id">ID of RPC request.</param>
        /// <returns>New <see cref="JsonRpcRequest" /> instance.</returns>
        [NotNull]
        public static JsonRpcRequest WithMethod(
            [NotNull] string method,
            [CanBeNull] IDictionary<string, object> namedParameters,
            [NotNull] JsonRpcId id)
        {
            if (namedParameters == null || namedParameters.Count == 0) {
                return new JsonRpcRequest(id, method);
            }

            return new ParametersDictionaryJsonRpcRequest(id, method, namedParameters);
        }

        #endregion

    }

}
