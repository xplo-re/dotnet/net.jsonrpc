﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections.Generic;
using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc
{

    /// <summary>
    ///     Provides a read-only collection of ordered parameter values.
    /// </summary>
    public interface IHasParametersCollection
    {

        /// <summary>
        ///     Immutable collection of parameters.
        /// </summary>
        [NotNull]
        [ItemCanBeNull]
        IReadOnlyCollection<object> Parameters { get; }

    }

}
