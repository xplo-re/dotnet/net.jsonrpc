﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

namespace XploRe.Net.JsonRpc
{

    /// <summary>
    ///     JSON-RPC message ID types.
    /// </summary>
    public enum JsonRpcIdType
    {

        /// <summary>
        ///     No JSON-RPC message ID given, i.e. the message is a notification.
        /// </summary>
        None = (int) default(JsonRpcIdType),

        /// <summary>
        ///     String JSON-RPC message ID.
        /// </summary>
        String = 1,

        /// <summary>
        ///     Numeric JSON-RPC message ID. The standard allows fractional parts, but their use is discouraged as the
        ///     representation of fractional parts may be inconsistent between systems.
        /// </summary>
        Number = 2,

        /// <summary>
        ///     Null JSON-RPC message ID. Although supported by the standard, the use in requests is discouraged.
        ///     Servers that receive malformed JSON requests or encounter and error detecting or decoding the JSON-RPC
        ///     ID must use the <see cref="Null" /> ID in responses.
        /// </summary>
        Null = 3

    }

}
