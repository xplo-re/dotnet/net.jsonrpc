﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using JetBrains.Annotations;


namespace XploRe.Net.JsonRpc
{

    /// <summary>
    ///     Base interface for JSON-RPC messages.
    /// </summary>
    public interface IJsonRpcMessage
    {

        /// <summary>
        ///     The JSON-RPC message ID.
        /// <para>
        ///     If a message has no message ID, the property equals the <see cref="F:XploRe.Net.JsonRpc.JsonRpcId.None" /> 
        ///     instance.
        /// </para>
        /// </summary>
        [NotNull]
        JsonRpcId Id { get; }

    }

}
