﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;


namespace XploRe.Net.JsonRpc
{

    /// <inheritdoc />
    /// <summary>
    ///     A batch of JSON-RPC messages that can be transmitted or received at once. 
    /// </summary>
    /// <typeparam name="T">The type of the JSON-RPC messages in this batch.</typeparam>
    public class JsonRpcBatch<T> : Collection<T> where T : IJsonRpcMessage
    {

        /// <inheritdoc />
        /// <summary>
        ///     Initialises an empty JSON-RPC messages batch.
        /// </summary>
        public JsonRpcBatch()
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new JSON-RPC batch with a given list of messages.
        /// </summary>
        /// <param name="collection">The list of JSON-RPC messages to initialise the batch with.</param>
        public JsonRpcBatch(IList<T> collection)
        {
            if (collection == null) {
                throw new ArgumentNullException(nameof(collection));
            }

            foreach (var item in collection.Select((value, index) => new { index, value })) {
                try {
                    Add(item.value);
                }
                catch (ArgumentNullException e) {
                    throw new ArgumentNullException(
                        e.Message + $" Item at position {item.index} in provided list is null.",
                        nameof(collection)
                    );
                }
            }
        }

        /// <inheritdoc />
        protected override void InsertItem(int index, T item)
        {
            if (item == null) {
                throw new ArgumentNullException(
                    nameof(item),
                    "A JSON-RPC batch cannot contain null values."
                );
            }

            base.InsertItem(index, item);
        }

        /// <inheritdoc />
        protected override void SetItem(int index, T item)
        {
            if (item == null) {
                throw new ArgumentNullException(
                    nameof(item),
                    "A JSON-RPC batch cannot contain null values."
                );
            }

            base.SetItem(index, item);
        }

    }

}
